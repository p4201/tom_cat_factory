﻿using Game.Core.Interfaces;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Core.Components.Timing
{
    [RequireComponent(typeof(StopwatchComponent))]
    public class TimerComponent : LoggedMonoBehaviour, IResettable
    {
        [Header("Configs")]
        [SerializeField] private int _SecondsCapacity;

        [Header("Components")]
        [SerializeField] private StopwatchComponent _Stopwatch = null;

        [Header("Events")]
        [SerializeField] private UnityEvent _OnStart = new UnityEvent();
        [SerializeField] private UnityEvent _OnUpdate = new UnityEvent();
        [SerializeField] private UnityEvent _OnFinish = new UnityEvent();

        private int _lastStopwatchTime;

        public int secondsPassed { get; private set; }
        public int secondsCapacity => _SecondsCapacity;
        public int secondsLeft => secondsCapacity - secondsPassed;
        public bool isActive { get; private set; }
        public bool isPaused { get; private set; }

        public UnityEvent onStart => _OnStart;
        public UnityEvent onUpdate => _OnUpdate;
        public UnityEvent onFinish => _OnFinish;

        ////////////////////////////////////////////////

        private void OnEnable()
        {
            if (isPaused)
                StartTimer();
        }

        private void OnDisable() => PauseTimer();

        private void Awake() =>
            _Stopwatch.onStop.AddListener(ResetStopwatchCache);

        private void OnDestroy()
        {
            StopTimer();
            _Stopwatch.onStop.RemoveListener(ResetStopwatchCache);
        }

        ////////////////////////////////////////////////

        public void SetCapacity(int secondsCapacity)
        {
            Log($"Set capacity: {secondsCapacity}");
            _SecondsCapacity = secondsCapacity;
            _OnUpdate?.Invoke();
        }

        public void StartTimer(int secondsCapacity)
        {
            SetCapacity(secondsCapacity);
            StartTimer();
        }

        [ContextMenu("Start Timer")]
        public void StartTimer()
        {
            if (isActive)
                return;

            if (!isPaused)
                ResetCount();

            Log("Start");
            isActive = true;
            isPaused = false;
            _Stopwatch.onUpdate.AddListener(OnStopwatchChanged);
            _Stopwatch.StartCounting();

            _OnStart?.Invoke();
            _OnUpdate?.Invoke();
        }

        [ContextMenu("Stop Timer")]
        public void StopTimer()
        {
            if (!isActive)
                return;

            Log("Stop");
            isActive = false;
            isPaused = false;
            _Stopwatch.StopCounting();
            _Stopwatch.onUpdate.RemoveListener(OnStopwatchChanged);
        }

        [ContextMenu("Pause Timer")]
        public void PauseTimer()
        {
            if (!isActive)
                return;

            Log("Pause");
            isActive = false;
            isPaused = true;
            _Stopwatch.PauseCounting();
            _Stopwatch.onUpdate.RemoveListener(OnStopwatchChanged);
        }

        [ContextMenu("Reset Timer")]
        public void Reset()
        {
            StopTimer();
            ResetCount();
            _Stopwatch.Reset();
        }

        private void ResetStopwatchCache() => _lastStopwatchTime = 0;
        private void ResetCount() => SetSecondsPassed(0);

        ////////////////////////////////////////////////

        private void OnStopwatchChanged(float diff)
        {
            var currentStopwatchTime = (int) _Stopwatch.currentTime;
            if (_lastStopwatchTime == currentStopwatchTime)
                return;

            var passedSeconds = currentStopwatchTime - _lastStopwatchTime;
            _lastStopwatchTime = currentStopwatchTime;

            SetSecondsPassed(secondsPassed + passedSeconds);
            TryFinish();
        }

        private void SetSecondsPassed(int count)
        {
            Log($"Second passed: {count}");
            secondsPassed = count;
            _OnUpdate?.Invoke();
        }

        private void TryFinish()
        {
            if (secondsLeft > 0)
                return;

            Log("Finished");
            StopTimer();
            _OnFinish?.Invoke();
        }
    }
}
