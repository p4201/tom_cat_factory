﻿using System.Collections;
using Game.Core.Extensions;
using Game.Core.Interfaces;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Core.Components.Timing
{
    public class StopwatchComponent : LoggedMonoBehaviour, IResettable
    {
        [Header("Events")]
        [SerializeField] private UnityEvent _OnStart = new UnityEvent();
        [SerializeField] private UnityEvent _OnStop = new UnityEvent();
        [SerializeField] private UnityEvent _OnPause = new UnityEvent();
        [SerializeField] private UnityEventFloat _OnUpdate = new UnityEventFloat();

        public bool isPaused { get; private set; }
        public bool isCounting { get; private set; }
        public float currentTime { get; private set; }

        public UnityEvent onStart => _OnStart;
        public UnityEvent onStop => _OnStop;
        public UnityEvent onPause => _OnPause;
        public UnityEventFloat onUpdate => _OnUpdate;

        private Coroutine _routine;

        ////////////////////////////////////////////////

        private void OnDisable() => StopCounting();

        ////////////////////////////////////////////////

        public void SetCurrentTime(float currentTime)
        {
            Log($"Set time: {currentTime}");
            this.currentTime = currentTime;
        }

        public void Reset()
        {
            Log("Reset");
            StopCounting();
        }

        ////////////////////////////////////////////////

        [ContextMenu("Start Counting")]
        public void StartCounting()
        {
            if (isPaused)
            {
                Log("Unpaused");
                isPaused = false;
                return;
            }

            if (isCounting)
                return;

            StopCounting();
            Log("Start Counting");

            isCounting = true;
            _routine = StartCoroutine(CountingRoutine());

            _OnStart?.Invoke();
        }

        [ContextMenu("Pause Counting")]
        public void PauseCounting()
        {
            if (!isCounting)
                return;

            if (isPaused)
                return;

            Log("Paused");
            isPaused = true;

            _OnPause?.Invoke();
        }

        [ContextMenu("Stop Counting")]
        public void StopCounting()
        {
            if (!isCounting)
                return;

            Log("Stop Counting");
            isPaused = false;
            isCounting = false;

            if (_routine != null)
                StopCoroutine(_routine);

            _routine = null;
            currentTime = 0;

            _OnStop?.Invoke();
        }

        ////////////////////////////////////////////////

        private IEnumerator CountingRoutine()
        {
            while (true)
            {
                if (!isPaused)
                {
                    var dt = Time.deltaTime;

                    _OnUpdate?.Invoke(dt);
                    currentTime += dt;
                }

                yield return null;
            }
            // ReSharper disable once IteratorNeverReturns
        }
    }
}
