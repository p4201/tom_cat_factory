﻿
namespace Game.Core.Components
{
    public class ValueStateCheckableMonoBehaviour : LoggedMonoBehaviour
    {
        private bool _wasModified { get; set; }

        private void OnEnable()
        {
            if (_wasModified)
                return;

            InitState();
            UpdateState();
        }

        private void Update()
        {
            if (HasStateChanged())
                UpdateState();
        }

        protected void SetModified()
            => _wasModified = true;

        protected virtual void InitState() { }
        protected virtual bool HasStateChanged() => false;
        protected virtual void UpdateState() { }
    }
}
