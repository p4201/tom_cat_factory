﻿using UnityEngine;

namespace Game.Core.Components
{
    public class LoggedClass
    {
        private bool _EnableLogs = true;

        private bool _canShow => _EnableLogs && (Debug.isDebugBuild || Application.isEditor);
        
        ////////////////////////////////////////////////

        protected void Log(string message)
        {
            if (_canShow)
                Debug.Log(WrapMessage(message));
        }

        protected void Error(string message)
        {
            if (_canShow)
                Debug.LogError(WrapMessage(message));
        }

        protected void Warning(string message)
        {
            if (_canShow)
                Debug.LogWarning(WrapMessage(message));
        }

        ////////////////////////////////////////////////

        private string WrapMessage(string message) => $"[{GetType().Name}] {message}";
    }
}
