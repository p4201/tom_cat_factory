﻿using System;
using System.Collections;
using Game.Core.Interfaces;
using UnityEngine;

namespace Game.Core.Components
{
    [RequireComponent(typeof(IDestroyable))]
    public class AutoDestroyable : CustomMonoBehaviour
    {
        [SerializeField] private bool _AutoStart = false;
        [SerializeField] private float _Timer = 0;

        private IDestroyable _destroyable;
        private Coroutine _timerActions { get; set; }

        public bool started => _timerActions != null;

        ///////////////////////////////////////////////

        private void OnEnable()
        {
            Initialize();

            if (_AutoStart)
                StartTimer();
        }

        private void OnDisable()
            => StopTimer();

        ///////////////////////////////////////////////

        public void StartTimer()
        {
            StopTimer();

            _timerActions = StartCoroutine(TimerActions(
                () => _timerActions = null));

            Log($"Started for {_Timer} seconds");
        }

        public void StopTimer()
        {
            if (_timerActions == null)
                return;

            StopCoroutine(_timerActions);
            _timerActions = null;
            Log("Stopped");
        }

        ///////////////////////////////////////////////

        private void Initialize()
            => TryAttach(ref _destroyable);

        private IEnumerator TimerActions(Action callback = null)
        {
            yield return new WaitForSeconds(_Timer);
            DestroyDestroyable();
            callback?.Invoke();
        }

        private void DestroyDestroyable()
        {
            _destroyable?.Destroy();
            Log("Destroy");
        }
    }
}
