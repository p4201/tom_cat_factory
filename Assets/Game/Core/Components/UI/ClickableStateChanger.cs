﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.Core.Components.UI
{
    public abstract class ClickableStateChanger : LoggedMonoBehaviour, IPointerDownHandler, IPointerExitHandler, IPointerUpHandler
    {
        [SerializeField] protected float _ClickStateMultiplier;

        private RectTransform _transformCache;
        private bool _clicked;
        private bool _cached;

        ////////////////////////////////////////////

        private void OnDisable() =>
            TryResetClickedState();

        public void OnPointerDown(PointerEventData eventData)
        {
            Log("Pointer Down");
            TryCache();

            _clicked = true;
            SetClickState();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            Log("Pointer Exit");
            TryResetClickedState();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Log("Pointer Up");
            TryResetClickedState();
        }

        ////////////////////////////////////////////

        protected abstract void CacheState();
        protected abstract void ResetState();
        protected abstract void SetClickState();

        protected RectTransform GetTransform()
        {
            if (_transformCache == null)
                _transformCache = GetComponent<RectTransform>();

            return _transformCache;
        }

        ////////////////////////////////////////////

        private void TryCache()
        {
            if (_cached)
                return;

            CacheState();
            _cached = true;
        }

        private void TryResetClickedState()
        {
            if (_cached && _clicked)
                ResetState();
        }
    }
}