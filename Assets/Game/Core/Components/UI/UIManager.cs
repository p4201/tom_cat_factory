﻿using System.Collections.Generic;
using System.Linq;
using Game.Core.Components.UI.Screens;
using Game.Core.DI;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Game.Core.Components.UI
{
    public class UIManager : LoggedMonoBehaviour
    {
        [Inject] private readonly PrefabInstanceCreator _PrefabCreator;

        [Header("Configs")]
        [SerializeField] private UIScreensLibrary _Library = null;

        [Header("Events")]
        [SerializeField] private UnityEvent _OnFirstScreenOpened = new UnityEvent();
        [SerializeField] private UnityEvent _OnLastScreenClosed = new UnityEvent();
        [SerializeField] private UnityEvent _OnScreenOpened = new UnityEvent();
        [SerializeField] private UnityEvent _OnScreenClosed = new UnityEvent();

        private Dictionary<ScreenType, ScreenController> _screensCache = new Dictionary<ScreenType, ScreenController>();
        private LinkedList<ScreenController> _screensQueue = new LinkedList<ScreenController>();

        public UnityEvent onFirstScreenOpened => _OnFirstScreenOpened;
        public UnityEvent onLastScreenClosed => _OnLastScreenClosed;
        public UnityEvent onScreenOpened => _OnScreenOpened;
        public UnityEvent onScreenClosed => _OnScreenClosed;

        //////////////////////////////////////////////////////

        private void Awake()
            => Initialize();

        //////////////////////////////////////////////////////

        private void Initialize()
        {
            foreach (var screen in GetComponentsInChildren<ScreenController>(true))
                AddToCache(screen);
        }

        private void AddToCache(ScreenController screen)
        {
            screen.onOpened.AddListener(() => OnScreenOpened(screen));
            screen.onClosed.AddListener(() => OnScreenClosed(screen));
            screen.onHided.AddListener(() => OnScreenHided(screen));

            if (screen.opened)
                _screensQueue.AddLast(screen);

            _screensCache.Add(screen.type, screen);
        }

        //////////////////////////////////////////////////////

        private void OnScreenOpened(ScreenController screen)
        {
            if (screen.countedAsFirst && !_screensQueue.Any(s => s.countedAsFirst))
                _OnFirstScreenOpened?.Invoke();

            _OnScreenOpened?.Invoke();

            if (TryFindFromLast(screen, out var node))
                _screensQueue.Remove(node);

            _screensQueue.AddLast(screen);

            HideExcept(screen);
        }

        private void OnScreenClosed(ScreenController screen)
        {
            OnScreenHided(screen);

            if (TryFindFromLast(screen, out var node))
            {
                var prevScreen = node.Previous?.Value;
                _screensQueue.Remove(node);

                prevScreen?.Open();
            }
        }

        private void OnScreenHided(ScreenController screen)
        {
            if (screen.countedAsLast && _screensQueue.Count(s => s.countedAsLast) == 1)
                _OnLastScreenClosed?.Invoke();

            _OnScreenClosed?.Invoke();
        }

        //////////////////////////////////////////////////////

        public ScreenController Open(ScreenType screenType)
        {
            var screen = GetScreen(screenType);
            screen?.Open();

            return screen;
        }

        [ContextMenu("Close All")]
        public void CloseAll()
            => CloseExcept();

        public bool ContainsInQueue(ScreenType type) =>
            _screensQueue.Any(s => s.type == type);

        //////////////////////////////////////////////////////

        private ScreenController GetScreen(ScreenType screenType)
        {
            if (_screensCache.TryGetValue(screenType, out var screen))
                return screen;

            return CreateScreen(screenType);
        }

        private ScreenController CreateScreen(ScreenType screenType)
        {
            if (_Library.TryGetPrefab(screenType, out var prefab) == false)
            {
                Error($"Library doesn't contain {screenType}");
                return null;
            }

            Log($"Instantiated {screenType}");
            var obj = _PrefabCreator.Instantiate(prefab, transform);

            if (obj.TryGetComponent<ScreenController>(out var screen) == false)
            {
                Error($"Can't get {nameof(ScreenController)} on {screenType}");
                return null;
            }

            AddToCache(screen);
            return screen;
        }

        private void CloseExcept(params ScreenController[] exceptions)
        {
            foreach (var screen in _screensCache.Values.Except(exceptions).ToArray())
                screen.Close();
        }

        private void HideExcept(params ScreenController[] exceptions)
        {
            foreach (var screen in _screensQueue.Except(exceptions).ToArray())
                screen.Hide();
        }

        private bool TryFindFromLast(ScreenController searchingValue, out LinkedListNode<ScreenController> resultNode)
        {
            resultNode = _screensQueue.Last;

            while (resultNode?.Value != null && resultNode.Value != searchingValue)
                resultNode = resultNode.Previous;

            return resultNode?.Value != null;
        }
    }
}
