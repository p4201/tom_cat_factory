﻿using Game.Core.Components.UI.Views;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Game.Core.Components.UI.Screens
{
    [RequireComponent(typeof(CanvasGroup))]
    public class ScreenController : ValueStateCheckableMonoBehaviour
    {
        #region SERIALIZE_FIELDS

        [Header("Components")]
        [SerializeField] private Button[] _CloseButtons = new Button[0];

        [Header("Settings")]
        [SerializeField] private ScreenType _Type = ScreenType.None;
        [SerializeField] private bool _CountedAsFirst = true;
        [SerializeField] private bool _CountedAsLast = true;
        [SerializeField] private bool _CanBeHided = false;

        [Header("State")]
        [SerializeField] private bool _Opened = false;
        [SerializeField] private bool _Hided = false;

        [Header("Events")]
        [SerializeField] private UnityEvent _OnOpened = new UnityEvent();
        [SerializeField] private UnityEvent _OnClosed = new UnityEvent();
        [SerializeField] private UnityEvent _OnHided = new UnityEvent();

        #endregion // SERIALIZE_FIELDS

        #region PRIVATE_FIELDS

        private IView _view_Cache;
        private IView _view {
            get {
                if (_view_Cache == null) {
                    _view_Cache = new ViewByCoroutine(this, GetComponent<CanvasGroup>());
                    _view_Cache.onPlayOpenStarted += () => gameObject.SetActive(true);
                    _view_Cache.onPlayCloseFinished += () => gameObject.SetActive(false);
                }
                return _view_Cache;
            }
        }

        private bool _lastState { get; set; }

        #endregion // PRIVATE_FIELDS

        #region PUBLIC_FIELDS
        public ScreenType type => _Type;
        public bool countedAsFirst => _CountedAsFirst;
        public bool countedAsLast => _CountedAsLast;
        public bool canBeHided => _CanBeHided;
        public bool opened => _Opened;
        public bool hided => _Hided;

        public UnityEvent onOpened {
            get => _OnOpened;
            set => _OnOpened = value;
        }
        public UnityEvent onClosed {
            get => _OnClosed;
            set => _OnClosed = value;
        }
        public UnityEvent onHided
        {
            get => _OnHided;
            set => _OnHided = value;
        }

        #endregion // PUBLIC_FIELDS

        /////////////////////////////////////////////////

        #region STATE_CHECKING

        protected override void InitState() => _lastState = !_Opened;
        protected override bool HasStateChanged() => _lastState != _Opened;
        protected override void UpdateState() => SetOpened(_Opened, _Hided);

        #endregion // STATE_CHECKING

        /////////////////////////////////////////////////

        private void OnEnable()
        {
            for (var i = 0; i < _CloseButtons.Length; i++)
                _CloseButtons[i].onClick.AddListener(Close);
        }

        private void OnDisable()
        {
            for (var i = 0; i < _CloseButtons.Length; i++)
                _CloseButtons[i].onClick.RemoveListener(Close);
        }

        /////////////////////////////////////////////////

        #region PUBLIC_METHODS

        [ContextMenu("Open")]
        public void Open()
            => SetOpened(true, false);

        [ContextMenu("Hide")]
        public void Hide()
            => SetOpened(false, true);

        [ContextMenu("Close")]
        public void Close()
            => SetOpened(false, false);

        #endregion // PUBLIC_METHODS

        #region PRIVATE_METHODS
        private void SetOpened(bool status, bool asHiding)
        {
            if (_lastState == status)
                return;

            _Opened = status;
            _lastState = status;
            _Hided = !status && asHiding && _CanBeHided;

            SetModified();

            GetEventToInvoke(asHiding)?.Invoke();

            if (_Opened) _view.PlayOpen();
            else _view.PlayClose();

            Log(GetLogMessage(asHiding));
        }

        private UnityEvent GetEventToInvoke(bool asHided)
        {
            switch(GetStatus(asHided))
            {
                case 0: return _OnClosed;
                case 1: return _OnHided;
                case 2: return _OnOpened;
                default: return null;
            }
        }

        private string GetLogMessage(bool asHidden)
        {
            switch(GetStatus(asHidden))
            {
                case 0: return "Closed";
                case 1: return "Hidden";
                case 2: return "Opened";
                default: return "";
            }
        }

        private int GetStatus(bool asHidden)
        {
            if (_Opened)
                return 2;

            if (asHidden && _CanBeHided)
                return 1;

            return 0;
        }

        #endregion // PRIVATE_METHODS
    }
}
