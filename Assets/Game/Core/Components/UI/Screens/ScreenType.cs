﻿
namespace Game.Core.Components.UI.Screens
{
    public enum ScreenType
    {
        None = 0,
        EntryPoint,
        MainMenu,
        Meta,
        Tasks,
        LevelsMenu,
        LevelStart,
        LevelGame,
        LevelPause,
        LevelFail,
        LevelComplete,
    }
}
