﻿using UnityEngine;

namespace Game.Core.Components.UI.Screens
{
    [RequireComponent(typeof(ScreenController))]
    public class ScreenBehaviour : CustomMonoBehaviour
    {
        private ScreenController _screenControllerCache;

        public ScreenController screen => _screenControllerCache ?
            _screenControllerCache : _screenControllerCache = GetComponent<ScreenController>();
    }
}
