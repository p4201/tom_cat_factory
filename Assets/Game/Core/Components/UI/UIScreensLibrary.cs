﻿using System;
using System.Collections.Generic;
using Game.Core.Components.UI.Screens;
using UnityEngine;

namespace Game.Core.Components.UI
{
    [CreateAssetMenu(fileName = nameof(UIScreensLibrary), menuName = "Definitions/UI/" + nameof(UIScreensLibrary))]
    public class UIScreensLibrary : ScriptableObject
    {
        [SerializeField] private ViewPrefabByType[] _Elements = new ViewPrefabByType[0];

        public IReadOnlyList<ViewPrefabByType> elements => _Elements;

        ////////////////////////////////////////////

        public bool TryGetPrefab(ScreenType screenType, out GameObject prefab)
        {
            for (var i = 0; i < _Elements.Length; i++)
                if (_Elements[i].type == screenType)
                {
                    prefab = _Elements[i].prefab;
                    return true;
                }

            prefab = null;
            return false;
        }

        public GameObject GetPrefab(ScreenType screenType)
        {
            TryGetPrefab(screenType, out var prefab);
            return prefab;
        }

        public bool HasPrefab(ScreenType screenType)
        {
            for (var i = 0; i < _Elements.Length; i++)
                if (_Elements[i].type == screenType)
                    return true;

            return false;
        }
    }

    ////////////////////////////////////////////

    [Serializable]
    public struct ViewPrefabByType
    {
        [SerializeField] private ScreenType _Type;
        [SerializeField] private GameObject _Prefab;

        public ScreenType type => _Type;
        public GameObject prefab => _Prefab;

        ////////////////////////////////////////////

        private ViewPrefabByType(ScreenType type, GameObject prefab)
        {
            _Type = type;
            _Prefab = prefab;
        }
    }
}
