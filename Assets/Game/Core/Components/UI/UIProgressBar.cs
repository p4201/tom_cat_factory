﻿using System.Collections;
using System.Threading.Tasks;
using Game.Core.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Core.Components.UI
{
    [RequireComponent(typeof(Slider)), ExecuteAlways]
    public class UIProgressBar : CustomMonoBehaviour
    {
        private const float _RATIO_EPS = 0.01f;
        private const float _MIN_RATIO = 0f;
        private const float _MAX_RATIO = 1f;

        [Header("Components")]
        [SerializeField] private Slider _Slider = null;
        [SerializeField] private CompositeText _Text = null;

        [Header("Values")]
        [SerializeField, Range(_MIN_RATIO, _MAX_RATIO)] private float _Ratio = 0f;

        [Header("Smoothness config")]
        [SerializeField, Min(0)] private float _RatioPerSecond = 0f;

        [Header("Events")]
        [SerializeField] private UnityEventFloat _Updated = new UnityEventFloat();

        private float _ratioCache = -1f;
        private Coroutine _smoothRoutine;

        public float ratio => _Ratio;
        public UnityEventFloat updated => _Updated;

        //////////////////////////////////////////////////////

        private void Update()
        {
            // если вдруг поменяли значение в инспекторе,
            // то нужно засинкать этот факт с логикой
            if (_Ratio.EqualsApprox(_ratioCache, _RATIO_EPS))
                return;

            SetRatio(_Ratio);
        }

        //////////////////////////////////////////////////////

        public void SetRatio(float ratio)
        {
            _Ratio = _ratioCache = Mathf.Clamp(ratio, _MIN_RATIO, _MAX_RATIO);
            UpdateSlider();
        }

        public Task SetRatioSmooth(float ratio)
        {
            if (_smoothRoutine != null)
                StopCoroutine(_smoothRoutine);

            var completionTask = new TaskCompletionSource<bool>();

            _smoothRoutine = StartCoroutine(
                SmoothRatio(_Ratio, ratio, _RatioPerSecond, completionTask));

            return completionTask.Task;
        }

        private void UpdateSlider()
        {
            Attach(ref _Slider).value = _Ratio;
            _Text?.SetPart((int) (_Ratio * 100f));

            _Updated?.Invoke(_Ratio);
        }

        //////////////////////////////////////////////////////

        private IEnumerator SmoothRatio(float startRatio, float targetRatio, float speed, TaskCompletionSource<bool> completionTask)
        {
            var ratio = startRatio;

            while (ratio < targetRatio)
            {
                var dt = Time.deltaTime;
                var dr = speed * dt;
                ratio += dr;

                SetRatio(ratio);
                yield return null;
            }

            completionTask.SetResult(true);
        }
    }
}
