﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.Core.Components.UI
{
    public class ClickableElement : MonoBehaviour
    {
        [ContextMenu("Click")]
        public void Click()
        {
            if (TryGetComponent<Button>(out var button))
                button.onClick.Invoke();
        }
    }
}