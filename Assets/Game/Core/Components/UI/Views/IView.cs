﻿using UnityEngine.Events;

namespace Game.Core.Components.UI.Views
{
    public interface IView
    {
        event UnityAction onPlayOpenStarted;
        event UnityAction onPlayOpenFinished;
        event UnityAction onPlayCloseStarted;
        event UnityAction onPlayCloseFinished;

        void PlayOpen();
        void PlayClose();
    }
}
