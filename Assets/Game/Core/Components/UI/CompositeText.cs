﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Core.Components.UI
{
    [RequireComponent(typeof(UniversalText))]
    public class CompositeText : CustomMonoBehaviour
    {
        [SerializeField] private string _Text = "";
        [SerializeField] private string[] _Parts = new string[0];

        private UniversalText _universalText;

        public string text => _Text;
        public IReadOnlyList<string> parts => _Parts;

        ////////////////////////////////////////////////////////

        private void Awake() => TryAttach(ref _universalText);
        private void OnValidate() => UpdateText();

        ////////////////////////////////////////////////////////

        public void SetText(string text)
        {
            _Text = text;
            UpdateText();
        }

        public void SetPart(string part) =>
            SetPart(part, 0);

        public void SetPart(string part, int index)
        {
            if (_Parts == null)
            {
                _Parts = new string[index + 1];
            }
            else if (_Parts.Length <= index)
            {
                var newArray = new string[index + 1];
                _Parts.CopyTo(newArray, 0);
                _Parts = newArray;
            }

            _Parts[index] = part;
            CorrectParts();
            UpdateText();
        }

        public void SetParts(params string[] parts)
        {
            _Parts = parts;
            CorrectParts();
            UpdateText();
        }

        #region OVERLOADING

        public void SetParts(params int[] values)
        {
            for (var i = 0; i < values.Length; i++)
                SetPart(values[i], i);
        }

        public void SetPart(int value, int index) => SetPart(value.ToString(), index);
        public void SetPart(int value) => SetPart(value.ToString());
        public void SetText(int value) => SetText(value.ToString());

        #endregion // OVERLOADING

        ////////////////////////////////////////////////////////

        private void UpdateText()
        {
            if (_Text == null || _Parts == null || !TryAttach(ref _universalText))
                return;

            _universalText.SetText(string.Format(_Text, _Parts));
        }

        private void CorrectParts()
        {
            if (_Parts == null)
                _Parts = new string[0];

            for (var i = 0; i < _Parts.Length; i++)
                if (_Parts[i] == null)
                    _Parts[i] = "";
        }
    }
}