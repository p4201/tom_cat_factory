﻿using UnityEngine;

namespace Game.Core.Components
{
    public class DebugComponent : MonoBehaviour
    {
        private void OnEnable()
        {
            if (!Debug.isDebugBuild)
                gameObject.SetActive(false);
        }
    }
}