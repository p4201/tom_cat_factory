﻿namespace Game.Core.Components
{
    public class CustomMonoBehaviour : LoggedMonoBehaviour
    {
        protected bool TryAttach<T>(ref T component) =>
            Attach(ref component) != null;

        protected T Attach<T>(ref T component)
        {
            if (component == null)
                TryGetComponent(out component);

            return component;
        }
    }
}
