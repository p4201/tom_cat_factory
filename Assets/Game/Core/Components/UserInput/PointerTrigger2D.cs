﻿using UnityEngine;
using UnityEngine.Events;

namespace Game.Core.Components.UserInput
{
    [RequireComponent(typeof(Collider2D))]
    public class PointerTrigger2D : LoggedMonoBehaviour
    {
        [Header("Status")]
        public bool active = true;

        [Header("Events")]
        [SerializeField] private UnityEvent _OnPointerUp = new UnityEvent();
        [SerializeField] private UnityEvent _OnPointerDown = new UnityEvent();
        [SerializeField] private UnityEvent _OnPointerClick = new UnityEvent();
        [SerializeField] private UnityEvent _OnPointerCancel = new UnityEvent();

        private Collider2D _colliderCache;
        private Collider2D _collider => _colliderCache ?? (_colliderCache = GetComponent<Collider2D>());

        public UnityEvent onPointerUp => _OnPointerUp;
        public UnityEvent onPointerDown => _OnPointerDown;
        public UnityEvent onPointerClick => _OnPointerClick;
        public UnityEvent onPointerCancel => _OnPointerCancel;

        ////////////////////////////////////////////////

        public void SetActive(bool active)
        {
            if (this.active == active)
                return;

            Log($"Set active: {active}");
            _collider.enabled = active;
            this.active = active;
        }

        ////////////////////////////////////////////////

        public void MouseDown()
        {
            Log("Pointer Down");
            _OnPointerDown?.Invoke();
        }

        public void MouseUp()
        {
            Log("Pointer Up");
            _OnPointerUp?.Invoke();
        }

        public void MouseClick()
        {
            Log("Click");
            _OnPointerClick?.Invoke();
        }

        public void MouseCancel()
        {
            Log("Cancel");
            _OnPointerCancel?.Invoke();
        }
    }
}
