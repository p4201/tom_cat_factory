﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Game.Core.Components.UserInput
{
    public class PointerRaycaster : LoggedMonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private Camera _Camera = null;

        [Header("Configs")]
        [SerializeField] private float _Distance = 0f;
        [SerializeField] private LayerMask _LayerMask = new LayerMask();

        private PointerTrigger2D _currentObject = null;

        private bool _canHandle => Time.timeScale > 0
                                   && EventSystem.current.IsPointerOverGameObject() == false
                                   && EventSystem.current.currentSelectedGameObject == null;

        ////////////////////////////////////////////////

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
                HandleMouseDown();

            else if (Input.GetMouseButtonUp(0))
                HandleMouseUp();
        }

        ////////////////////////////////////////////////

        private void HandleMouseDown()
        {
            Log("Mouse Down");

            if (!_canHandle)
                return;

            var clickedObject = GetRaycastObject();
            if (clickedObject == _currentObject)
                return;

            _currentObject?.MouseCancel();
            clickedObject?.MouseDown();

            _currentObject = clickedObject;
        }

        private void HandleMouseUp()
        {
            Log("Mouse Up");

            if (!_canHandle)
                return;

            var clickedObject = GetRaycastObject();
            clickedObject?.MouseUp();

            if (clickedObject == _currentObject)
                _currentObject?.MouseClick();
            else
                _currentObject?.MouseCancel();

            _currentObject = null;
        }

        ////////////////////////////////////////////////

        private PointerTrigger2D GetRaycastObject()
        {
            var mousePos2D = (Vector2) _Camera.ScreenToWorldPoint(UnityEngine.Input.mousePosition);
            var hit = Physics2D.Raycast(mousePos2D, Vector2.zero, _Distance, _LayerMask);

            if (hit.collider == null)
                return null;

            if (hit.collider.gameObject.TryGetComponent<PointerTrigger2D>(out var pointer) == false)
                return null;

            Log($"Hit {hit.collider.gameObject.name}");
            return pointer;
        }
    }
}
