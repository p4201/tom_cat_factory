﻿using UnityEngine;

namespace Game.Core.Components
{
    public partial class CameraFitter
    {
        private void OnDrawGizmos()
        {
            var position = transform.position;

            var leftDown = position - (Vector3) _ScreenSize / 2f;
            var leftUp = leftDown + Vector3.up * _ScreenSize.y;
            var rightUp = position + (Vector3) _ScreenSize / 2f;
            var rightDown = rightUp + Vector3.down * _ScreenSize.y;

            Gizmos.color = Color.green;
            Gizmos.DrawLine(leftDown, leftUp);
            Gizmos.DrawLine(leftUp, rightUp);
            Gizmos.DrawLine(rightUp, rightDown);
            Gizmos.DrawLine(rightDown, leftDown);
        }
    }
}
