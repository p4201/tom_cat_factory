﻿using UnityEngine;

namespace Game.Core.Components
{
    [RequireComponent(typeof(Camera))]
    public partial class CameraFitter : LoggedMonoBehaviour
    {
        [Header("Configs")]
        [SerializeField] private bool _AutoStart = true;
        [SerializeField] private Vector2 _ScreenSize = Vector2.zero;

        public Vector2 screenSize => _ScreenSize;

        ////////////////////////////////////////////////

        private void Start()
        {
            if (_AutoStart)
                Fit();
        }

        ////////////////////////////////////////////////

        [ContextMenu("Fit")]
        public void Fit()
        {
            if (TryGetComponent<Camera>(out var camera) == false)
                return;

            if (TryCalcResultScreenSize(camera, out var size) == false)
                return;

            camera.orthographicSize = 0.5f * size.y;
        }

        private bool TryCalcResultScreenSize(Camera camera, out Vector2 resultSize)
        {
            var ratio = (float) camera.pixelWidth / camera.pixelHeight;

            var targetWidth = _ScreenSize.x;
            var targetHeight = _ScreenSize.y;

            var targetWidthByHeight = ratio * targetHeight;
            var targetHeightByWidth = targetWidth / ratio;

            if (targetHeight <= targetHeightByWidth)
            {
                resultSize = new Vector2(targetWidth, targetHeightByWidth);
                return true;
            }

            else if (targetWidth <= targetWidthByHeight)
            {
                resultSize = new Vector2(targetWidthByHeight, targetHeight);
                return true;
            }

            resultSize = Vector2.zero;
            return false;
        }
    }
}
