﻿using System.Collections.Generic;
using Game.Core.Extensions;
using Game.Core.Interfaces;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Core.Components
{
    public class SortingLayerChanger : LoggedMonoBehaviour, IResettable
    {
        [Header("Components")]
        [SerializeField] private SpriteRenderer _SpriteRenderer = null;

        [Header("Config")]
        public bool cycled = false;
        [SerializeField] private string[] _Layers = new string[0];

        [Header("Events")]
        [SerializeField] private UnityEvent _OnChangeLayer = new UnityEvent();


        public IReadOnlyList<string> layers => _Layers;
        public int currentLayerIndex { get; private set; }
        public string currentLayer => _Layers.IsNullOrEmpty() ? "" : _Layers[currentLayerIndex];
        public UnityEvent onChangeLayer => _OnChangeLayer;

        ////////////////////////////////////////////////

        public void Reset()
        {
            Log("Reset");
            SetLayer(0);
        }

        ////////////////////////////////////////////////

        [ContextMenu("Set Next Layer")]
        public void SetNextLayer() => SetLayer(GetNextIndex());

        [ContextMenu("Set Prev Layer")]
        public void SetPrevLayer() => SetLayer(GetPrevIndex());

        public void SetLayer(int index)
        {
            if (ValidateIndex(index) == false)
                return;

            Log($"Set index: {index}");
            currentLayerIndex = index;
            _SpriteRenderer.sortingLayerName = currentLayer;

            _OnChangeLayer?.Invoke();
        }

        ////////////////////////////////////////////////

        private bool ValidateIndex(int index) =>
            index >= 0 && index < _Layers.Length;

        private int GetNextIndex()
        {
            if (_Layers.IsNullOrEmpty())
                return currentLayerIndex;

            var result = currentLayerIndex + 1;
            if (!cycled)
                return result;

            while (result >= _Layers.Length)
                result -= _Layers.Length;

            return result;
        }

        private int GetPrevIndex()
        {
            if (_Layers.IsNullOrEmpty())
                return currentLayerIndex;

            var result = currentLayerIndex - 1;
            if (!cycled)
                return result;

            while (result < 0)
                result += _Layers.Length;

            return result;
        }
    }
}
