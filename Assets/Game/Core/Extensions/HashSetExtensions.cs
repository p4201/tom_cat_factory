﻿using System.Collections.Generic;
using System.Linq;

namespace Game.Core.Extensions
{
    public static class HashSetExtensions
    {
        public static bool IsNullOrEmpty<T>(this HashSet<T> hashSet) =>
            hashSet == null || !hashSet.Any();


        public static void AddRange<T>(this HashSet<T> hashSet, IEnumerable<T> elements)
        {
            foreach (var element in elements)
                hashSet.Add(element);
        }
    }
}
