﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Core.Extensions
{
    [Serializable] public class UnityEventBool : UnityEvent<bool> { }
    [Serializable] public class UnityEventInt : UnityEvent<int> { }
    [Serializable] public class UnityEventFloat : UnityEvent<float> { }
    [Serializable] public class UnityEventVector2 : UnityEvent<Vector2> { }
}
