﻿namespace Game.Core.Extensions
{
    public static class ArrayExtensions
    {
        public static bool IsNullOrEmpty<T>(this T[] array) => array == null || array.Length == 0;

        public static int SafeLength<T>(this T[] array) => array?.Length ?? 0;

        public static bool TryGet<T>(this T[] array, int index, out T element)
        {
            if (index < 0 || index >= array.SafeLength())
            {
                element = default;
                return false;
            }

            element = array[index];
            return true;
        }
    }
}