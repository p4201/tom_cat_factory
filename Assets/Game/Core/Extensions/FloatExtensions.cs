﻿using UnityEngine;

namespace Game.Core.Extensions
{
    public static class FloatExtensions
    {
        public static bool EqualsApprox(this float value1, float value2, float eps) =>
            Mathf.Abs(value1 - value2) < eps;
    }
}
