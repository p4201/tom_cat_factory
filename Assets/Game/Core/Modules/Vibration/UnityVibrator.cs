﻿using Game.Core.Components;
using UnityEngine;

namespace Game.Core.Modules.Vibration
{
    internal class UnityVibrator : LoggedClass, IVibrator
    {
        /// <param name="milliseconds"> Это фэйк для наследования </param>
        public void Vibrate(long milliseconds = 250)
        {
            Handheld.Vibrate();
            Log($"Vibrate: {milliseconds}");
        }

        public void Stop()
        {
            Log("Stop");
        }
    }
}