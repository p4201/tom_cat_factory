﻿using Core.Modules;
using Game.Core.Components;

namespace Game.Core.Modules.Vibration
{
    internal class EmptyVibrator : LoggedClass, IVibrator
    {
        public void Vibrate(long milliseconds = 250) =>
            Log($"Vibrate: {milliseconds}");

        public void Stop() =>
            Log("Stop");
    }
}