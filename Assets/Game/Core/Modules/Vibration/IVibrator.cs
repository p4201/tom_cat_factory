﻿namespace Game.Core.Modules.Vibration
{
    public interface IVibrator
    {
        void Vibrate(long milliseconds = 250);
        void Stop();
    }
}