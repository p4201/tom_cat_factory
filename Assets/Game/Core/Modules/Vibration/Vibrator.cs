﻿namespace Game.Core.Modules.Vibration
{
    public class Vibrator : IVibrator
    {
        private IVibrator _concreteVibrator;

        //////////////////////////////////////

        public Vibrator()
        {
#if UNITY_EDITOR
            _concreteVibrator = new EmptyVibrator();
#elif UNITY_ANDROID
            _concreteVibrator = new AndroidVibrator();
#else
            _concreteVibrator = new UnityVibrator();
#endif
        }

        //////////////////////////////////////

        public void Vibrate(long milliseconds = 250) =>
            _concreteVibrator.Vibrate(milliseconds);

        public void Stop() =>
            _concreteVibrator.Stop();
    }
}