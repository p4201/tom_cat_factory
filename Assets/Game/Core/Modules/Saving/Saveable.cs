﻿using System;
using Game.Core.Components;
using Game.Core.Interfaces;

namespace Game.Core.Modules.Saving
{
    public interface ISaveData<out T> : ICloneable<T> { }

    public abstract class Saveable<T> : LoggedClass where T : ISaveData<T>
    {
        private readonly SaveManager _saveManager;
        private bool _hasUnsavedData;

        protected T data { get; private set; }
        public abstract string saveKey { get; }

        public event Action dataChanged;

        ////////////////////////////////////////////////

        protected Saveable(SaveManager saveManager)
        {
            _saveManager = saveManager;
        }

        ////////////////////////////////////////////////

        public void Init(T saveData)
        {
            Log("Init");
            data = saveData.Clone();
            dataChanged?.Invoke();

            OnInit();
        }

        public void Save()
        {
            if (_hasUnsavedData == false)
                return;

            Log("Saved");
            _hasUnsavedData = false;
            _saveManager.Save(saveKey, data);
        }

        ////////////////////////////////////////////////

        protected virtual void OnInit() { }

        protected void OnDataChanged()
        {
            _hasUnsavedData = true;
            dataChanged?.Invoke();
        }
    }
}
