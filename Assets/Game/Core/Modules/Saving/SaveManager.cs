﻿using Game.Core.Components;
using Game.Core.Extensions;
using UnityEngine;

namespace Game.Core.Modules.Saving
{
    public class SaveManager : LoggedClass
    {
        private readonly SerializationManager _serializationManager;
        private bool _hasUnsavedData;

        ////////////////////////////////////////////////

        public SaveManager(SerializationManager serializationManager)
        {
            _serializationManager = serializationManager;
            _hasUnsavedData = false;
        }

        ////////////////////////////////////////////////

        public void Save<T>(string saveKey, T saveData, bool hard = false) where T : ISaveData<T>
        {
            var serializedData = _serializationManager.Serialize(saveData);

            Log($"Save '{saveKey}'");
            PlayerPrefs.SetString(saveKey, serializedData);
            _hasUnsavedData = true;

            if (hard)
                HardSave();
        }

        public bool TryLoadData<T>(string saveKey, out T saveData) where T : ISaveData<T>
        {
            var serializedData = PlayerPrefs.GetString(saveKey, "");

            if (serializedData.IsNullOrEmpty())
            {
                Log($"Can't load '{saveKey}'");
                saveData = default;
                return false;
            }

            saveData = _serializationManager.Deserialize<T>(serializedData);
            if (saveData == null)
            {
                Log($"Can't load {saveKey}");
                return false;
            }

            Log($"Load '{saveKey}'");
            return true;
        }

        public void HardSave()
        {
            if (_hasUnsavedData == false)
                return;

            Log("Hard Save");
            PlayerPrefs.Save();
            _hasUnsavedData = false;
        }
    }
}
