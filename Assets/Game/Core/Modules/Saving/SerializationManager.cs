﻿using System;
using Game.Core.Components;
using Newtonsoft.Json;

namespace Game.Core.Modules.Saving
{
    public class SerializationManager : LoggedClass
    {
        public string Serialize<T>(T obj)
        {
            try
            {
                var json = JsonConvert.SerializeObject(obj);
                Log("Serialized");
                return json;
            }
            catch(Exception e)
            {
                Error($"Serialization Error: {e.Message}");
                return "";
            }
        }

        public T Deserialize<T>(string json)
        {
            try
            {
                var data = JsonConvert.DeserializeObject<T>(json);
                Log("Deserialized");
                return data;
            }
            catch(Exception e)
            {
                Error($"Deserialization error: {e.Message}");
                return default;
            }
        }
    }
}
