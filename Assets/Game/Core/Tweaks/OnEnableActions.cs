﻿using UnityEngine;
using UnityEngine.Events;

namespace Game.Core.Tweaks
{
    public class OnEnableActions : MonoBehaviour
    {
        [SerializeField] public UnityEvent onEnableEvent = new UnityEvent();

        ////////////////////////////////////////////////

        private void OnEnable()
            => onEnableEvent?.Invoke();
    }
}