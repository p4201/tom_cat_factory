﻿using UnityEngine;
using Zenject;

namespace Game.Core.DI
{
    public class PrefabInstanceCreator
    {
        private readonly DiContainer _container;

        ////////////////////////////////////////////////

        public PrefabInstanceCreator(DiContainer container) =>
            _container = container;

        ////////////////////////////////////////////////

        public T Instantiate<T>(T prefab) where T : Object => Instantiate(prefab, null);
        public T Instantiate<T>(GameObject prefab) where T : Object => Instantiate<T>(prefab, null);

        public T Instantiate<T>(GameObject prefab, Transform container) where T : Object =>
            Instantiate(prefab, container).GetComponent<T>();

        public T Instantiate<T>(T prefab, Transform container) where T : Object
        {
            var obj = Object.Instantiate(prefab, container);
            _container.Inject(obj);

            return obj;
        }
    }
}
