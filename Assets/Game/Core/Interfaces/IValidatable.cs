﻿namespace Game.Core.Interfaces
{
    public interface IValidatable
    {
        void Validate();
    }
}
