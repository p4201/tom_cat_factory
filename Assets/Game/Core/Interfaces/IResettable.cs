﻿namespace Game.Core.Interfaces
{
    public interface IResettable
    {
        void Reset();
    }
}
