﻿
namespace Game.Core.Interfaces
{
    public interface IDestroyable
    {
        void Destroy();
    }
}
