﻿using UnityEngine;

namespace Game.Core.Interfaces
{
    public interface IGameObjectHost
    {
        GameObject gameObject { get; }
    }
}
