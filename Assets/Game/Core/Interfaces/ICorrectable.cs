﻿namespace Game.Core.Interfaces
{
    public interface ICorrectable
    {
        void Correct();
    }
}
