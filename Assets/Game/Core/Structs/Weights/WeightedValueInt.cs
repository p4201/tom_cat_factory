﻿using System;
using UnityEngine;

namespace Game.Core.Structs.Weights
{
    [Serializable]
    public struct WeightedValueInt : IWeightedValue<int>
    {
        [SerializeField] private int _Value;
        [SerializeField] [Min(0)] private float _Weight;

        public int value => _Value;
        public float weight => _Weight;

        //////////////////////////////////////////

        public WeightedValueInt(int value)
        {
            _Value = value;
            _Weight = 0f;
        }

        public WeightedValueInt(int value, float weight) : this(value) =>
            _Weight = weight;
    }
}
