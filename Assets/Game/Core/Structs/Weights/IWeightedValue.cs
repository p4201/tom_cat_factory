﻿namespace Game.Core.Structs.Weights
{
    public interface IWeightedValue<out T>
    {
        T value { get; }
        float weight { get; }
    }
}
