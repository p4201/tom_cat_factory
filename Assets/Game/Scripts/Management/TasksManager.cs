﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Core.Extensions;
using Game.Core.Modules.Saving;
using Game.Scripts.Definitions.Tasks;
using Game.Scripts.Meta.Definitions;
using Newtonsoft.Json;

namespace Game.Scripts.Management
{
    [Serializable]
    public class TasksData : ISaveData<TasksData>
    {
        public HashSet<string> currentTasks;
        public HashSet<string> completedTasks;

        [JsonIgnore]
        public bool isEmpty => currentTasks.IsNullOrEmpty() && completedTasks.IsNullOrEmpty();

        ////////////////////////////////////////////////

        public TasksData()
        {
            currentTasks = new HashSet<string>();
            completedTasks = new HashSet<string>();
        }

        ////////////////////////////////////////////////

        public TasksData Clone()
        {
            var tasksData = new TasksData();

            tasksData.completedTasks.AddRange(completedTasks);
            tasksData.currentTasks.AddRange(currentTasks);

            return tasksData;
        }
    }

    public class TasksManager : Saveable<TasksData>
    {
        private const string _SAVE_KEY = "Tasks";

        private readonly TasksRoster _roster;
        private readonly StorageManager _storageManager;
        private readonly HashSet<TaskConfig> _currentTasks;

        public override string saveKey => _SAVE_KEY;
        public IReadOnlyCollection<TaskConfig> currentTasks => _currentTasks;

        public event Action onCurrentTasksUpdated;
        public event Action<TaskConfig> onTaskOpened;
        public event Action<TaskConfig> onTaskCompleted;

        ////////////////////////////////////////////////

        public TasksManager(SaveManager saveManager, TasksRoster roster, StorageManager storageManager)
            : base(saveManager)
        {
            _roster = roster;
            _storageManager = storageManager;
            _currentTasks = new HashSet<TaskConfig>();
        }

        protected override void OnInit()
        {
            if (data.isEmpty)
                foreach (var task in _roster.GetStartTasks())
                    AddCurrentTask(task);

            else
                foreach (var taskId in data.currentTasks)
                    if (_roster.TryGetTask(taskId, out var config))
                        AddCurrentTask(config);
        }

        ////////////////////////////////////////////////

        public bool IsTaskCompleted(TaskConfig task) => IsTaskCompleted(task.id);
        public bool IsTaskCompleted(string id) => data.completedTasks.Contains(id);

        public bool IsTaskCurrent(TaskConfig task) => IsTaskCurrent(task.id);
        public bool IsTaskCurrent(string id) => data.currentTasks.Contains(id);

        ////////////////////////////////////////////////

        public bool TryCompleteTask(string id)
        {
            if (_roster.TryGetTask(id, out var task) == false)
            {
                Error($"Can't find task with id {id}");
                return false;
            }

            return TryCompleteTask(task);
        }

        public bool TryCompleteTask(TaskConfig task)
        {
            if (_storageManager.HasEnough(task.cost) == false)
            {
                Log("Doesn't have enough valuables to complete task");
                return false;
            }

            Log($"Complete task {task.id}");

            _storageManager.Subtract(task.cost);

            RemoveCurrentTask(task);
            AddCompletedTasks(task);
            onTaskCompleted?.Invoke(task);

            TryOpenNextTasks(task);
            onCurrentTasksUpdated?.Invoke();

            OnDataChanged();
            _storageManager.Save();
            Save();

            return true;
        }

        ////////////////////////////////////////////////

        private bool TryOpenNextTasks(TaskConfig completedTask)
        {
            var opened = false;

            foreach (var task in completedTask.nextTasks)
                opened |= TryOpenTask(task);

            return opened;
        }

        private bool TryOpenTask(TaskConfig task)
        {
            if (CanTaskBeOpened(task) == false)
                return false;

            Log($"Open new task {task.id}");

            AddCurrentTask(task);
            onTaskOpened?.Invoke(task);

            return true;
        }
        private bool CanTaskBeOpened(TaskConfig task) =>
            !IsTaskCurrent(task) && !IsTaskCompleted(task) && task.previousTasks.All(IsTaskCompleted);

        ////////////////////////////////////////////////

        private void AddCurrentTask(TaskConfig task)
        {
            _currentTasks.Add(task);
            data.currentTasks.Add(task.id);
        }

        private void RemoveCurrentTask(TaskConfig task)
        {
            _currentTasks.Remove(task);
            data.currentTasks.Remove(task.id);
        }

        private void AddCompletedTasks(TaskConfig task)
        {
            data.completedTasks.Add(task.id);
        }

        private void RemoveCompletedTasks(TaskConfig task)
        {
            data.completedTasks.Remove(task.id);
        }
    }
}
