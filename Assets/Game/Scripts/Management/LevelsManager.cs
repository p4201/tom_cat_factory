﻿using System;
using Game.Core.Modules.Saving;
using Game.Scripts.Definitions;
using Game.Scripts.Level.Definitions.Level;
using UnityEngine;

namespace Game.Scripts.Management
{
    [Serializable]
    public class LevelsData : ISaveData<LevelsData>
    {
        public int currentIndex;

        ////////////////////////////////////////////////

        public LevelsData Clone() =>
            new LevelsData()
            {
                currentIndex = currentIndex
            };
    }

    public class LevelsManager : Saveable<LevelsData>
    {
        private const string _SAVE_KEY = "Levels";

        private readonly LevelsRoster _roster;

        public int currentIndex => data.currentIndex;
        public int nextIndex => currentIndex + 1;
        public int prevIndex => currentIndex - 1;

        public LevelConfig currentLevel => GetLevel(currentIndex);
        public LevelConfig nextLevel => GetLevel(nextIndex);
        public LevelConfig prevLevel => GetLevel(prevIndex);

        public int levelsCount => _roster.totalCount;
        public override string saveKey => _SAVE_KEY;

        ////////////////////////////////////////////////

        public LevelsManager(LevelsRoster roster, SaveManager saveManager) : base(saveManager)
        {
            _roster = roster;
        }

        ////////////////////////////////////////////////

        public LevelConfig GetLevel(int levelIndex) =>
            _roster.GetLevel(levelIndex);

        ////////////////////////////////////////////////

        public void SetNextLevel() => SetCurrentLevel(nextLevel);
        public void SetPrevLevel() => SetCurrentLevel(prevLevel);

        public void SetCurrentLevel(LevelConfig level)
        {
            if (_roster.TryGetIndex(level, out var index) == false)
                return;

            SetCurrentLevel(index);
        }

        public void SetCurrentLevel(int index, bool forced = false)
        {
            data.currentIndex = forced ? index : Mathf.Clamp(index, 0, _roster.totalCount - 1);
            Log($"Set Current Level {index}");
            OnDataChanged();
        }
    }
}
