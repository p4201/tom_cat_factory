﻿using Game.Core.Components;
using Game.Core.Extensions;
using Game.Core.Modules.Saving;
using Game.Scripts.Definitions;
using UnityEngine.SceneManagement;

namespace Game.Scripts.Management
{
    public class ScenesManager : LoggedClass
    {
        private readonly ScenesRoster _roster;
        private readonly SaveManager _saveManager;

        public string currentScene { get; private set; }
        public string lastScene { get; private set; }

        public bool isFirstScene => lastScene.IsNullOrEmpty();

        ////////////////////////////////////////////////

        public ScenesManager(ScenesRoster roster, SaveManager saveManager)
        {
            _roster = roster;
            _saveManager = saveManager;

            currentScene = SceneManager.GetActiveScene().name;
        }

        ////////////////////////////////////////////////

        public void LoadPrevScene() => LoadScene(lastScene);
        public void LoadMetaScene() => LoadScene(_roster.metaSceneName);
        public void LoadLevelScene() => LoadScene(_roster.levelSceneName);
        public void LoadMainMenuScene() => LoadScene(_roster.mainMenuSceneName);
        public void LoadEntryPointScene() => LoadScene(_roster.entryPointName);
        public void LoadLevelsMenuScene() => LoadScene(_roster.levelsMenuSceneName);

        ////////////////////////////////////////////////

        private void LoadScene(string sceneName)
        {
            if (currentScene == sceneName || sceneName.IsNullOrEmpty())
                return;

            lastScene = currentScene;
            currentScene = sceneName;

            Log($"Load {sceneName}");
            SceneManager.LoadScene(sceneName);

            _saveManager.HardSave();
        }
    }
}
