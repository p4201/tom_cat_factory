﻿using System;
using System.Collections.Generic;
using Game.Core.Extensions;
using Game.Core.Modules.Saving;
using Game.Scripts.Definitions.Valuables;

namespace Game.Scripts.Management
{
    [Serializable]
    public class StorageData : Dictionary<ValuableType, float>, ISaveData<StorageData>
    {
        public StorageData Clone()
        {
            var data = new StorageData();

            foreach (var keyValuePair in this)
                data.Add(keyValuePair.Key, keyValuePair.Value);

            return data;
        }
    }

    public class StorageManager : Saveable<StorageData>
    {
        private const float _COMPARE_FLOAT_EPS = 0.000001f;
        private const string _SAVE_KEY = "Storage";

        public override string saveKey => _SAVE_KEY;

        public event Action<ValuableType> typeChanged;

        ////////////////////////////////////////////////

        public StorageManager(SaveManager saveManager) : base(saveManager) { }

        ////////////////////////////////////////////////

        public bool HasEnough(IReadOnlyList<CountableValuableConfig> valuables)
        {
            for (var i = 0; i < valuables.Count; i++)
                if (HasEnough(valuables[i]) == false)
                    return false;

            return true;
        }

        public bool HasEnough(CountableValuableConfig valuable) =>
            HasEnough(valuable.valuable.type, valuable.count);

        public bool HasEnough(ValuableType type, float count) =>
            GetCount(type) >= count;

        public bool HasValuable(ValuableType type) =>
            GetCount(type) > 0;

        public float GetCount(ValuableType type) =>
            data.TryGetValue(type, out var value) ? value : 0f;

        ////////////////////////////////////////////////

        public void Add(IReadOnlyList<CountableValuableConfig> valuables)
        {
            for (var i = 0; i < valuables.Count; i++)
                Add(valuables[i]);
        }

        public void Add(CountableValuableConfig valuable) =>
            Add(valuable.valuable.type, valuable.count);

        public void Add(ValuableType type, float count) =>
            SetValue(type, GetCount(type) + count);

        ////////////////////////////////////////////////

        public void Subtract(IReadOnlyList<CountableValuableConfig> valuables)
        {
            for (var i = 0; i < valuables.Count; i++)
                Subtract(valuables[i]);
        }

        public void Subtract(CountableValuableConfig valuable) =>
            Subtract(valuable.valuable.type, valuable.count);

        public void Subtract(ValuableType type, float count) =>
            SetValue(type, GetCount(type) - count);

        ////////////////////////////////////////////////

        private void SetValue(ValuableType type, float value)
        {
            if (data.TryGetValue(type, out var dataValue) && dataValue.EqualsApprox(value, _COMPARE_FLOAT_EPS))
                return;

            Log($"Set {value} to {type}");

            if (value > 0)
                data[type] = value;

            if (value <= 0)
                data.Remove(type);

            OnDataChanged();
            typeChanged?.Invoke(type);
        }
    }
}
