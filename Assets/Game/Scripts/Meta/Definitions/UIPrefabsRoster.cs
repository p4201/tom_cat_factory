﻿using Game.Scripts.Definitions;
using UnityEngine;

namespace Game.Scripts.Meta.Definitions
{
    [CreateAssetMenu(fileName = nameof(UIPrefabsRoster), menuName = DefinitionsHelper.Meta.PATH + nameof(UIPrefabsRoster))]
    public class UIPrefabsRoster : ScriptableObject
    {
        [SerializeField] private GameObject _UITaskItem = null;

        public GameObject uiTaskItem => _UITaskItem;
    }
}
