﻿using System.Collections.Generic;
using Game.Core.Components.UI.Screens;
using Game.Core.DI;
using Game.Scripts.Definitions.Tasks;
using Game.Scripts.Management;
using Game.Scripts.Meta.Definitions;
using UnityEngine;
using Zenject;

namespace Game.Scripts.Meta.UI
{
    [RequireComponent(typeof(ScreenController))]
    public class UITasksScreen : ScreenBehaviour
    {
        [Inject] private readonly UIPrefabsRoster _prefabs;
        [Inject] private readonly TasksManager _tasksManager;
        [Inject] private readonly PrefabInstanceCreator _prefabInstantiator;

        [SerializeField] private GameObject _NoTasksLabel = null;
        [SerializeField] private Transform _ItemsContainer = null;

        private readonly List<UITaskItem> _items = new List<UITaskItem>();

        ////////////////////////////////////////////////

        private void OnEnable() => Init();
        private void OnDisable() => Deinit();

        ////////////////////////////////////////////////

        private void Init()
        {
            Deinit();

            var noTasks = true;

            foreach (var task in _tasksManager.currentTasks)
            {
                var item = _prefabInstantiator.Instantiate<UITaskItem>(_prefabs.uiTaskItem, _ItemsContainer);
                item.Init(task, OnItemClicked);
                _items.Add(item);
                noTasks = false;
            }

            _NoTasksLabel.SetActive(noTasks);
        }

        private void Deinit()
        {
            for (var i = 0; i < _items.Count; i++)
            {
                _items[i].Reset();
                DestroyImmediate(_items[i].gameObject);
            }

            _items.Clear();
        }

        ////////////////////////////////////////////////

        private void OnItemClicked(TaskConfig config)
        {
            if (_tasksManager.TryCompleteTask(config) == false)
                return;

            Init();
        }
    }
}
