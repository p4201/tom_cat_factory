﻿using System.Collections.Generic;
using Game.Core.Components.UI;
using Game.Core.Components.UI.Screens;
using Game.Scripts.Level.Definitions.Level;
using Game.Scripts.Management;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.Scripts.Meta.UI.Screens
{
    [RequireComponent(typeof(ScreenController))]
    public class UIPlayGameScreen : ScreenBehaviour
    {
        [Inject] private readonly LevelsManager _LevelsManager;
        [Inject] private readonly ScenesManager _ScenesManager;

        [Header("Components")]
        [SerializeField] private UniversalText _LevelIndex = null;
        [SerializeField] private UILevelTarget[] _Targets = new UILevelTarget[0];

        [SerializeField] private Button _PLayLevelButton = null;

        ////////////////////////////////////////////////

        private void OnEnable()
        {
            _PLayLevelButton.onClick.AddListener(OnClickPlayLevel);
        }

        private void OnDisable()
        {
            _PLayLevelButton.onClick.RemoveListener(OnClickPlayLevel);
        }

        ////////////////////////////////////////////////

        public void SetLevel(int levelIndex)
        {
            Log($"Set level {levelIndex + 1}");
            var levelConfig = _LevelsManager.GetLevel(levelIndex);

            SetHeader(levelIndex);
            SetTargets(levelConfig.boxes.boxes);
        }

        ////////////////////////////////////////////////

        private void SetHeader(int levelIndex)
        {
            _LevelIndex.SetText(levelIndex + 1);
        }

        private void SetTargets(IReadOnlyList<BoxLevelConfig> boxConfigs)
        {
            ClearTargets();
            _Targets = new UILevelTarget[boxConfigs.Count];

            var counter = 0;
            foreach (var boxConfig in boxConfigs)
            {
                var target = CreateTarget();
                target.Init(boxConfig.targetItem.icon, boxConfig.capacity);
                _Targets[counter++] = target;
            }
        }

        ////////////////////////////////////////////////

        private void ClearTargets()
        {
            
        }

        ////////////////////////////////////////////////

        private void OnClickPlayLevel()
        {
            Log("Play Level Click");
            _ScenesManager.LoadLevelScene();
        }
    }
}
