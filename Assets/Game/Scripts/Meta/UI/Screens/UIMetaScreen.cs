﻿using Game.Core.Components.UI;
using Game.Core.Components.UI.Screens;
using Game.Scripts.Management;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.Scripts.Meta.UI
{
    [RequireComponent(typeof(ScreenController))]
    public class UIMetaScreen : ScreenBehaviour
    {
        [Inject] private readonly UIManager _UIManager;
        [Inject] private readonly LevelsManager _LevelsManager;
        [Inject] private readonly ScenesManager _ScenesManager;

        [SerializeField] private Button _TasksButon = null;
        [SerializeField] private Button _AllLevelsButton = null;
        [SerializeField] private UILevelButton _LevelButton = null;

        ////////////////////////////////////////////////

        private void OnEnable()
        {
            _LevelButton.Init(_LevelsManager.currentIndex);

            _TasksButon.onClick.AddListener(OnTasksClick);
            _LevelButton.onClick.AddListener(OnLevelClick);
            _AllLevelsButton.onClick.AddListener(OnAllLevelsClick);
        }

        private void OnDisable()
        {
            _TasksButon.onClick.RemoveListener(OnTasksClick);
            _LevelButton.onClick.RemoveListener(OnLevelClick);
            _AllLevelsButton.onClick.RemoveListener(OnAllLevelsClick);
        }

        ////////////////////////////////////////////////

        private void OnTasksClick()
        {
            Log("Tasks click");
            _UIManager.Open(ScreenType.Tasks);
        }

        private void OnLevelClick()
        {
            Log("Level Click");
            _ScenesManager.LoadLevelScene();
        }

        private void OnAllLevelsClick()
        {
            Log("All Levels Click");
            _ScenesManager.LoadLevelsMenuScene();
        }
    }
}
