﻿using Game.Core.Components;
using Game.Core.Components.UI;
using Game.Core.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Meta.UI
{
    public class UITaskCost : LoggedMonoBehaviour, IResettable
    {
        [SerializeField] private Image _Icon = null;
        [SerializeField] private UniversalText _Count = null;

        ////////////////////////////////////////////////

        public void Init(Sprite icon, int count)
        {
            _Icon.sprite = icon;
            _Count.SetText(count);
        }

        public void Reset()
        {
            _Icon.sprite = null;
            _Count.Reset();
        }
    }
}
