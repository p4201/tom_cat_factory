﻿using Game.Core.Components;
using Game.Core.Components.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Meta.UI
{
    public class UILevelButton : LoggedMonoBehaviour
    {
        [SerializeField] private UniversalText _LevelText = null;
        [SerializeField] private Button _ButtonComponent = null;

        public Button.ButtonClickedEvent onClick => _ButtonComponent.onClick;

        ////////////////////////////////////////////////

        public void Init(int levelIndex)
        {
            Log("Init");
            _LevelText.SetText(levelIndex + 1);
        }
    }
}
