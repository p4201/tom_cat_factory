﻿using Game.Core.Components;
using Game.Core.Components.UI;
using Game.Core.Interfaces;
using Game.Scripts.Definitions.Tasks;
using Game.Scripts.Meta.Definitions;
using Game.Scripts.Meta.Extensions;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Game.Scripts.Meta.UI
{
    public class UITaskItem : LoggedMonoBehaviour, IResettable
    {
        [Header("Components")]
        [SerializeField] private Image _Icon = null;
        [SerializeField] private UITaskCost _Cost = null;
        [SerializeField] private UniversalText _Title = null;
        [SerializeField] private UniversalText _Description = null;
        [SerializeField] private Button _Button = null;

        [Header("Events")]
        [SerializeField] private UnityEventTaskConfig _Clicked = null;


        [ShowInInspector, ReadOnly]
        public TaskConfig config { get; private set; }

        public UnityEventTaskConfig clicked => _Clicked;

        ////////////////////////////////////////////////

        private void OnDestroy() => Reset();

        ////////////////////////////////////////////////

        public void Init(TaskConfig config, UnityAction<TaskConfig> onClick)
        {
            Init(config);
            clicked.AddListener(onClick);
        }

        public void Init(TaskConfig config)
        {
            this.config = config;

            _Icon.sprite = config.icon;
            _Title.SetText(config.title);
            _Description.SetText(config.description);
            _Cost.Init(config.cost.valuable.icon, config.cost.count);

            _Button.onClick.RemoveAllListeners();
            _Button.onClick.AddListener(OnClick);

            clicked.RemoveAllListeners();
        }

        public void Reset()
        {
            _Icon.sprite = null;
            _Title.Reset();
            _Description.Reset();
            _Cost.Reset();

            _Button.onClick.RemoveAllListeners();
            clicked.RemoveAllListeners();
        }

        ////////////////////////////////////////////////

        private void OnClick() => clicked?.Invoke(config);
    }
}
