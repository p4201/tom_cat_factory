﻿using System;
using Game.Scripts.Definitions.Tasks;
using Game.Scripts.Meta.Definitions;
using UnityEngine.Events;

namespace Game.Scripts.Meta.Extensions
{
    [Serializable]
    public class UnityEventTaskConfig : UnityEvent<TaskConfig> { }
}
