﻿using Game.Core.Components.UI;
using Game.Core.DI;
using UnityEngine;
using Zenject;

namespace Game.Scripts.Meta.Installers
{
    public class SceneObjectsInstaller : MonoInstaller<SceneObjectsInstaller>
    {
        [SerializeField] private GameObject _UIManager = null;

        ////////////////////////////////////////////////

        public override void InstallBindings()
        {
            Container.Bind<UIManager>().FromComponentOn(_UIManager).AsSingle();
            Container.Bind<PrefabInstanceCreator>().FromInstance(new PrefabInstanceCreator(Container)).AsSingle();
        }
    }
}
