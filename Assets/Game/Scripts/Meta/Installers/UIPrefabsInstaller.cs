﻿using Game.Scripts.Meta.Definitions;
using UnityEngine;
using Zenject;

namespace Game.Scripts.Meta.Installers
{
    public class UIPrefabsInstaller : MonoInstaller<UIPrefabsInstaller>
    {
        [SerializeField] private UIPrefabsRoster _Roster = null;

        ////////////////////////////////////////////////

        public override void InstallBindings()
        {
            Container.Bind<UIPrefabsRoster>().FromInstance(_Roster).AsSingle();
        }
    }
}
