﻿using Game.Scripts.Definitions;
using Game.Scripts.Management;
using UnityEngine;
using Zenject;

namespace Game.Scripts.Installers
{
    public class ScenesManagementInstaller : MonoInstaller<ScenesManagementInstaller>
    {
        [SerializeField] private ScenesRoster _ScenesRoster = null;

        ////////////////////////////////////////////////

        public override void InstallBindings()
        {
            Container.Bind<ScenesRoster>().FromInstance(_ScenesRoster).AsSingle();
            Container.Bind<ScenesManager>().AsSingle();
        }
    }
}
