﻿using Game.Scripts.Management;
using Zenject;

namespace Game.Scripts.Installers
{
    public class StorageManagementInstaller : MonoInstaller<StorageManagementInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<StorageManager>().AsSingle();
        }
    }
}
