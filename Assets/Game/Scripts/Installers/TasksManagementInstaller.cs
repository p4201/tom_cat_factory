﻿using Game.Scripts.Definitions.Tasks;
using Game.Scripts.Management;
using Game.Scripts.Meta.Definitions;
using UnityEngine;
using Zenject;

namespace Game.Scripts.Installers
{
    public class TasksManagementInstaller : MonoInstaller<TasksManagementInstaller>
    {
        [SerializeField] private TasksRoster _TasksToster = null;

        ////////////////////////////////////////////////

        public override void InstallBindings()
        {
            Container.Bind<TasksRoster>().FromInstance(_TasksToster).AsSingle();
            Container.Bind<TasksManager>().AsSingle();
        }
    }
}
