﻿using Game.Core.Modules.Saving;
using Game.Scripts.Definitions.DefaultData;
using Game.Scripts.Management;
using UnityEngine;
using Zenject;

namespace Game.Scripts.Installers
{
    public class SaveManagementInstaller : MonoInstaller<SaveManagementInstaller>
    {
        [SerializeField] private DefaultDataRoster _DefaultDataRoster = null;

        ////////////////////////////////////////////////

        public override void InstallBindings()
        {
            Container.Bind<DefaultDataRoster>().FromInstance(_DefaultDataRoster).AsSingle();

            Container.Bind<SerializationManager>().AsSingle();
            Container.Bind<SaveManager>().AsSingle();
        }
    }
}
