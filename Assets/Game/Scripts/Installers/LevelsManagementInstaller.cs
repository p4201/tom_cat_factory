﻿using Game.Scripts.Definitions;
using Game.Scripts.Management;
using UnityEngine;
using Zenject;

namespace Game.Scripts.Installers
{
    public class LevelsManagementInstaller : MonoInstaller<LevelsManagementInstaller>
    {
        [SerializeField] private LevelsRoster _LevelsRoster = null;

        ////////////////////////////////////////////////

        public override void InstallBindings()
        {
            Container.Bind<LevelsRoster>().FromInstance(_LevelsRoster).AsSingle();
            Container.Bind<LevelsManager>().AsSingle();
        }
    }
}
