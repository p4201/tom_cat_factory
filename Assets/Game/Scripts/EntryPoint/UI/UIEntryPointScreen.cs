﻿using Game.Core.Components.UI;
using Game.Core.Components.UI.Screens;
using UnityEngine;

namespace Game.Scripts.EntryPoint.UI
{
    [RequireComponent(typeof(ScreenController))]
    public class UIEntryPointScreen : ScreenBehaviour
    {
        [SerializeField] private UIProgressBar _ProgressBar = null;

        public UIProgressBar progressBar => _ProgressBar;
    }
}
