﻿using Game.Core.Components.UI;
using Game.Core.DI;
using UnityEngine;
using Zenject;

namespace Game.Scripts.EntryPoint.Installers
{
    public class EntryPointInstaller : MonoInstaller<EntryPointInstaller>
    {
        [SerializeField] private GameObject _UIManager = null;

        ////////////////////////////////////////////////

        public override void InstallBindings()
        {
            Container.Bind<PrefabInstanceCreator>().FromInstance(new PrefabInstanceCreator(Container)).AsSingle();
            Container.Bind<UIManager>().FromComponentOn(_UIManager).AsSingle();
        }
    }
}
