﻿using System.Threading.Tasks;
using Game.Core.Components;
using Game.Core.Components.UI;
using Game.Core.Components.UI.Screens;
using Game.Core.Modules.Saving;
using Game.Scripts.Definitions.DefaultData;
using Game.Scripts.EntryPoint.UI;
using Game.Scripts.Management;
using Zenject;

namespace Game.Scripts.EntryPoint.Management
{
    public class EntryPointManager : LoggedMonoBehaviour
    {
        [Inject] private readonly UIManager _UIManager;
        [Inject] private readonly SaveManager _SaveManager;
        [Inject] private readonly DefaultDataRoster _DefaultDataRoster;

        [Inject] private readonly TasksManager _TasksManager;
        [Inject] private readonly ScenesManager _ScenesManager;
        [Inject] private readonly LevelsManager _LevelsManager;
        [Inject] private readonly StorageManager _StorageManager;

        ////////////////////////////////////////////////

        private async void Start()
        {
            var progressBar = _UIManager.Open(ScreenType.EntryPoint)
                .GetComponent<UIEntryPointScreen>().progressBar;

            await progressBar.SetRatioSmooth(0f);
            await Task.Delay(400);

            LoadData(_TasksManager);
            await progressBar.SetRatioSmooth(0.2f);
            await Task.Delay(200);

            LoadData(_StorageManager);
            await progressBar.SetRatioSmooth(0.5f);
            await Task.Delay(500);

            LoadData(_LevelsManager);
            await progressBar.SetRatioSmooth(1f);
            await Task.Delay(200);

            _ScenesManager.LoadMainMenuScene();
        }

        ////////////////////////////////////////////////

        private void LoadData<T>(Saveable<T> saveable) where T : ISaveData<T>, new()
        {
            if (_SaveManager.TryLoadData<T>(saveable.saveKey, out var data) == false)
                data = GetDefaultSaveData<T>();

            saveable.Init(data);
        }

        private T GetDefaultSaveData<T>() where T : ISaveData<T>
        {
            if (_DefaultDataRoster.TryGetDefaultDataContainer<T>(out var container) == false)
            {
                Error($"Can't load default {typeof(T)} data");
                return default;
            }

            Log($"Load default {typeof(T)} data");
            return container.GetDefaultData();
        }
    }
}
