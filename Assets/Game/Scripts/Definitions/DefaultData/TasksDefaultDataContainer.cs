﻿using System;
using System.Collections.Generic;
using Game.Core.Extensions;
using Game.Scripts.Definitions.Tasks;
using Game.Scripts.Management;
using Game.Scripts.Meta.Definitions;
using UnityEngine;

namespace Game.Scripts.Definitions.DefaultData
{
    [Serializable]
    public class TasksDefaultDataContainer : DefaultDataContainer<TasksData>
    {
        [SerializeField] private TaskConfig[] _CurrentTasks = new TaskConfig[0];

        ////////////////////////////////////////////////

        public override TasksData GetDefaultData()
        {
            var data = new TasksData();

            foreach (var task in _CurrentTasks)
            {
                AddPreviousTasksToSet(ref data.completedTasks, task);

                // в настройке конфига можно ошибиться
                if (data.completedTasks.Contains(task.id) == false)
                    data.currentTasks.Add(task.id);
            }

            return data;
        }

        private void AddPreviousTasksToSet(ref HashSet<string> hashSet, TaskConfig task)
        {
            foreach (var prevTask in task.previousTasks)
            {
                hashSet.Add(prevTask.id);
                AddPreviousTasksToSet(ref hashSet, prevTask);
            }
        }
    }
}
