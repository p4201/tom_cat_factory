﻿using Game.Core.Modules.Saving;
using UnityEngine;

namespace Game.Scripts.Definitions.DefaultData
{
    [CreateAssetMenu(fileName = nameof(DefaultDataRoster), menuName = DefinitionsHelper.PATH + nameof(DefaultDataRoster))]
    public class DefaultDataRoster : ScriptableObject
    {
        [SerializeField] private TasksDefaultDataContainer _TasksData = null;
        [SerializeField] private LevelsDefaultDataContainer _LevelData = null;
        [SerializeField] private StorageDefaultDataContainer _StorageData = null;

        public TasksDefaultDataContainer tasksDataContainer => _TasksData;
        public LevelsDefaultDataContainer levelsDataContainer => _LevelData;
        public StorageDefaultDataContainer storageDataContainer => _StorageData;

        private IDefaultDataContainer[] _containers => new IDefaultDataContainer[] { _TasksData, _LevelData, _StorageData };

        ////////////////////////////////////////////////

        public bool TryGetDefaultDataContainer<T>(out DefaultDataContainer<T> defaultDataContainer) where T : ISaveData<T>
        {
            var type = typeof(T);

            foreach (var container in _containers)
                if (type == container.saveDataType)
                {
                    defaultDataContainer = container as DefaultDataContainer<T>;
                    return true;
                }

            defaultDataContainer = default;
            return false;
        }
    }
}
