﻿using System;
using Game.Core.Modules.Saving;

namespace Game.Scripts.Definitions.DefaultData
{
    public interface IDefaultDataContainer
    {
        Type saveDataType { get; }
    }

    public abstract class DefaultDataContainer<TSaveData> : IDefaultDataContainer where TSaveData : ISaveData<TSaveData>
    {
        public Type saveDataType => typeof(TSaveData);

        public abstract TSaveData GetDefaultData();
    }
}
