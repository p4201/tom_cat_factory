﻿using System;
using Game.Scripts.Definitions.Valuables;
using Game.Scripts.Management;
using UnityEngine;

namespace Game.Scripts.Definitions.DefaultData
{
    [Serializable]
    public class StorageDefaultDataContainer : DefaultDataContainer<StorageData>
    {
        [SerializeField] private CountableValuableConfig[] _Values = new CountableValuableConfig[0];

        ////////////////////////////////////////////////

        public override StorageData GetDefaultData()
        {
            var data = new StorageData();

            foreach (var value in _Values)
                data.Add(value.valuable.type, value.count);

            return data;
        }
    }
}
