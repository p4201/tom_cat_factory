﻿using System;
using Game.Scripts.Management;
using UnityEngine;

namespace Game.Scripts.Definitions.DefaultData
{
    [Serializable]
    public class LevelsDefaultDataContainer : DefaultDataContainer<LevelsData>
    {
        [SerializeField] private int _LevelIndex = 0;

        ////////////////////////////////////////////////

        public override LevelsData GetDefaultData()
        {
            var data = new LevelsData
            {
                currentIndex = _LevelIndex
            };

            return data;
        }
    }
}
