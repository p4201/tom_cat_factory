﻿using System;
using UnityEngine;

namespace Game.Scripts.Definitions.Speed
{
    [Serializable]
    public class CurvedSpeed : ISpeed
    {
        [SerializeField] private AnimationCurve _SpeedByTime;

        ////////////////////////////////////////////////

        public CurvedSpeed(float speed = 0) : this(new AnimationCurve(new Keyframe(0, speed))) { }
        public CurvedSpeed(AnimationCurve speedByTime) =>
            _SpeedByTime = speedByTime;

        ////////////////////////////////////////////////

        public float GetCurrent(float timeSec) => _SpeedByTime.Evaluate(timeSec);
    }
}
