﻿namespace Game.Scripts.Definitions.Speed
{
    public interface ISpeed
    {
        float GetCurrent(float timeSec);
    }
}
