﻿namespace Game.Scripts.Definitions.Speed
{
    public enum SpeedType
    {
        Const,
        Accelerated,
        Curved
    }
}
