﻿using System;
using UnityEngine;

namespace Game.Scripts.Definitions.Speed
{
    [Serializable]
    public class AcceleratedSpeed : ISpeed
    {
        [Header("Main params")]
        [SerializeField] private float _StartSpeed;
        [SerializeField] private float _Acceleration;

        [Header("Optional params")]
        [SerializeField] private float _MinSpeed = _MIN_SPEED;
        [SerializeField] private float _MaxSpeed = _MAX_SPEED;


        private const float _MIN_SPEED = float.NegativeInfinity;
        private const float _MAX_SPEED = float.PositiveInfinity;

        ////////////////////////////////////////////////

        public AcceleratedSpeed(float acceleration = 0, float minSpeed = _MIN_SPEED, float maxSpeed = _MAX_SPEED) 
            : this(0, acceleration, minSpeed, maxSpeed) { }

        public AcceleratedSpeed(float startSpeed = 0, float acceleration = 0, float minSpeed = _MIN_SPEED, float maxSpeed = _MAX_SPEED)
        {
            _StartSpeed = startSpeed;
            _Acceleration = acceleration;
            _MinSpeed = minSpeed;
            _MaxSpeed = maxSpeed;
        }

        ////////////////////////////////////////////////

        public float GetCurrent(float timeSec) =>
            Mathf.Clamp(_StartSpeed + _Acceleration * timeSec, _MinSpeed, _MaxSpeed);
    }
}
