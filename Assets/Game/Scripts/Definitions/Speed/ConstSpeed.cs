﻿using System;
using UnityEngine;

namespace Game.Scripts.Definitions.Speed
{
    [Serializable]
    public class ConstSpeed : ISpeed
    {
        [SerializeField] private float _Speed;

        ////////////////////////////////////////////////

        public ConstSpeed(float speed = 0) =>
            _Speed = speed;

        ////////////////////////////////////////////////

        public float GetCurrent(float timeSec) => _Speed;
    }
}
