﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Definitions.Speed
{
    [Serializable]
    public class SpeedConfig
    {
        [SerializeField] private SpeedType _SpeedType = SpeedType.Const;

        [SerializeField, HideLabel, ShowIf(nameof(_SpeedType), SpeedType.Const)]
        private ConstSpeed _ConstSpeed = null;

        [SerializeField, HideLabel, ShowIf(nameof(_SpeedType), SpeedType.Curved)]
        private CurvedSpeed _CurvedSpeed = null;

        [SerializeField, HideLabel, ShowIf(nameof(_SpeedType), SpeedType.Accelerated)]
        private AcceleratedSpeed _AcceleratedSpeed = null;


        public ISpeed speed
        {
            get
            {
                switch (_SpeedType)
                {
                    case SpeedType.Const: return _ConstSpeed;
                    case SpeedType.Curved: return _CurvedSpeed;
                    case SpeedType.Accelerated: return _AcceleratedSpeed;
                    default: return new ConstSpeed();
                }
            }
        }

        ////////////////////////////////////////////////

        private SpeedConfig(ConstSpeed speed)
        {
            _SpeedType = SpeedType.Const;
            _ConstSpeed = speed;
        }

        private SpeedConfig(CurvedSpeed speed)
        {
            _SpeedType = SpeedType.Curved;
            _CurvedSpeed = speed;
        }

        private SpeedConfig(AcceleratedSpeed speed)
        {
            _SpeedType = SpeedType.Accelerated;
            _AcceleratedSpeed = speed;
        }
    }
}
