﻿using System;
using Game.Core.Interfaces;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Definitions.Direction
{
    [Serializable, HideLabel]
    public class DirectionConfig : IValidatable
    {
        [SerializeField] private Vector2 _Direction = Vector2.zero;

        public Vector2 direction => _Direction;

        ////////////////////////////////////////////////

        private DirectionConfig(Vector2 direction) =>
            _Direction = direction;

        ////////////////////////////////////////////////

        public void Validate() => _Direction.Normalize();
    }
}
