﻿using System.Collections.Generic;
using Game.Core.Extensions;
using Game.Scripts.Level.Definitions.Level;
using UnityEngine;

namespace Game.Scripts.Definitions
{
    [CreateAssetMenu(fileName = nameof(LevelsRoster), menuName = DefinitionsHelper.PATH + nameof(LevelsRoster))]
    public class LevelsRoster : ScriptableObject
    {
        [SerializeField] private LevelConfig[] _Levels = new LevelConfig[0];

        public IReadOnlyList<LevelConfig> levels => _Levels;
        public int totalCount => _Levels.Length;

        ////////////////////////////////////////////////

        public LevelConfig GetLevel(int index) =>
            TryGetLevel(index, out var level) ? level : null;

        public bool TryGetLevel(int index, out LevelConfig level) =>
            _Levels.TryGet(index, out level);

        ////////////////////////////////////////////////

        public int GetIndex(LevelConfig level)
        {
            for (var i = 0; i < _Levels.Length; i++)
                if (_Levels[i] == level)
                    return i;

            return -1;
        }

        public bool TryGetIndex(LevelConfig level, out int index) =>
            (index = GetIndex(level)) >= 0;
    }
}
