﻿using System.Collections.Generic;
using Game.Core.Extensions;
using Game.Scripts.Definitions.Valuables;
using UnityEngine;

namespace Game.Scripts.Definitions.Tasks
{
    [CreateAssetMenu(fileName = nameof(TaskConfig), menuName = DefinitionsHelper.Meta.PATH + nameof(TaskConfig))]
    public class TaskConfig : ScriptableObject
    {
        [Header("Info")]
        [SerializeField] private string _Id = "";
        [SerializeField] private string _Title = "";
        [SerializeField] private string _Description = "";
        [SerializeField] private Sprite _Icon = null;

        [Header("Storage")]
        [SerializeField] private CountableValuableConfig _Cost = default;
        [SerializeField] private CountableValuableConfig[] _Rewards = new CountableValuableConfig[0];

        [Header("Queue")]
        [SerializeField] private TaskConfig[] _NextTasks = new TaskConfig[0];
        [SerializeField] private TaskConfig[] _PreviousTasks = new TaskConfig[0];

        ////////////////////////////////////////////////

        public string id => _Id;
        public string title => _Title;
        public string description => _Description;
        public Sprite icon => _Icon;

        public CountableValuableConfig cost => _Cost;
        public IReadOnlyList<CountableValuableConfig> rewards => _Rewards;

        public IReadOnlyList<TaskConfig> nextTasks => _NextTasks;
        public IReadOnlyList<TaskConfig> previousTasks => _PreviousTasks;

        ////////////////////////////////////////////////

        public bool isLast => _NextTasks.IsNullOrEmpty();
        public bool isStart => _PreviousTasks.IsNullOrEmpty();
    }
}
