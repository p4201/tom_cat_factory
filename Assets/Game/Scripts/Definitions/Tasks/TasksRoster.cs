﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Scripts.Definitions.Tasks
{
    [CreateAssetMenu(fileName = nameof(TasksRoster), menuName = DefinitionsHelper.Meta.PATH + nameof(TasksRoster))]
    public class TasksRoster : ScriptableObject
    {
        [SerializeField] private TaskConfig[] _Tasks = new TaskConfig[0];

        public IReadOnlyList<TaskConfig> tasks => _Tasks;

        ////////////////////////////////////////////////

        public bool TryGetTask(string id, out TaskConfig task)
        {
            for (var i = 0; i < _Tasks.Length; i++)
                if (_Tasks[i].id == id)
                {
                    task = _Tasks[i];
                    return true;
                }

            task = null;
            return false;
        }

        public TaskConfig[] GetStartTasks() =>
            _Tasks.Where(t => t.isStart).ToArray();

        public TaskConfig[] GetLastTasks() =>
            _Tasks.Where(t => t.isLast).ToArray();
    }
}
