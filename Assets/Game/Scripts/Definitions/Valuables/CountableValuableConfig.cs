﻿using System;
using UnityEngine;

namespace Game.Scripts.Definitions.Valuables
{
    [Serializable]
    public struct CountableValuableConfig
    {
        [SerializeField] private ValuableConfig _Valuable;
        [SerializeField, Min(0)] private int _Count;

        public ValuableConfig valuable => _Valuable;
        public int count => _Count;

        ////////////////////////////////////////////////

        public CountableValuableConfig(ValuableConfig valuable, int count)
        {
            _Valuable = valuable;
            _Count = count;
        }
    }
}
