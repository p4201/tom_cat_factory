﻿namespace Game.Scripts.Definitions.Valuables
{
    public enum ValuableType
    {
        None = 0,
        Star = 1,
        Gold = 2
    }
}
