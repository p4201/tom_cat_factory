﻿using UnityEngine;

namespace Game.Scripts.Definitions.Valuables
{
    [CreateAssetMenu(fileName = nameof(ValuableConfig), menuName = DefinitionsHelper.PATH + nameof(ValuableConfig))]
    public class ValuableConfig : ScriptableObject
    {
        [SerializeField] private ValuableType _Type = ValuableType.None;
        [SerializeField] private string _Name = "";
        [SerializeField] private Sprite _Icon = null;

        public ValuableType type => _Type;
        public string name => _Name;
        public Sprite icon => _Icon;
    }
}
