﻿using UnityEngine;

namespace Game.Scripts.Definitions
{
    [CreateAssetMenu(fileName = nameof(ScenesRoster), menuName = DefinitionsHelper.PATH + nameof(ScenesRoster))]
    public class ScenesRoster : ScriptableObject
    {
        [SerializeField] private string _EntryPointName = string.Empty;
        [SerializeField] private string _MainMenuSceneName = string.Empty;
        [SerializeField] private string _MetaSceneName = string.Empty;
        [SerializeField] private string _LevelSceneName = string.Empty;
        [SerializeField] private string _LevelsMenuSceneName = string.Empty;

        public string entryPointName => _EntryPointName;
        public string mainMenuSceneName => _MainMenuSceneName;
        public string metaSceneName => _MetaSceneName;
        public string levelSceneName => _LevelSceneName;
        public string levelsMenuSceneName => _LevelsMenuSceneName;
    }
}
