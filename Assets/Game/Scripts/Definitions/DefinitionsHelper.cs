﻿namespace Game.Scripts.Definitions
{
    internal static class DefinitionsHelper
    {
        public const string PATH = "Definitions/";
        public const string UI_PATH = PATH + "UI/";

        internal static class Level
        {
            public const string PATH = DefinitionsHelper.PATH + "Level/";
        }

        internal static class LevelsMenu
        {
            public const string PATH = DefinitionsHelper.PATH + "LevelsMenu/";
        }

        internal static class Meta
        {
            public const string PATH = DefinitionsHelper.PATH + "Meta/";
        }
    }
}
