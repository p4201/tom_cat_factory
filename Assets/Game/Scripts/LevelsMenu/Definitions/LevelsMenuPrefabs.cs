﻿using Game.Scripts.Definitions;
using Game.Scripts.LevelsMenu.UI;
using UnityEngine;

namespace Game.Scripts.LevelsMenu.Definitions
{
    [CreateAssetMenu(fileName = nameof(LevelsMenuPrefabs), menuName = DefinitionsHelper.LevelsMenu.PATH + nameof(LevelsMenuPrefabs))]
    public class LevelsMenuPrefabs : ScriptableObject
    {
        [SerializeField] private UILevelsMenuGridItem _LevelsMenuGridItem = null;

        public UILevelsMenuGridItem levelsMenuGridItem => _LevelsMenuGridItem;
    }
}
