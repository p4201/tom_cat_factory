﻿using Game.Core.DI;
using Game.Scripts.LevelsMenu.Definitions;
using UnityEngine;
using Zenject;

namespace Game.Scripts.LevelsMenu
{
    public class LevelsMenuInstaller : MonoInstaller<LevelsMenuInstaller>
    {
        [SerializeField] private LevelsMenuPrefabs _Prefabs = null;

        ////////////////////////////////////////////////

        public override void InstallBindings()
        {
            Container.Bind<PrefabInstanceCreator>().FromInstance(new PrefabInstanceCreator(Container)).AsSingle();
            Container.Bind<LevelsMenuPrefabs>().FromInstance(_Prefabs).AsSingle();
        }
    }
}
