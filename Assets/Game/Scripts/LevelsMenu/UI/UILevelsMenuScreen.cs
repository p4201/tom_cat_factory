﻿using System.Collections.Generic;
using Game.Core.Components.UI.Screens;
using Game.Core.DI;
using Game.Scripts.LevelsMenu.Definitions;
using Game.Scripts.Management;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.Scripts.LevelsMenu.UI
{
    [RequireComponent(typeof(ScreenController))]
    public class UILevelsMenuScreen : ScreenBehaviour
    {
        [Inject] private readonly ScenesManager _ScenesManager;
        [Inject] private readonly LevelsManager _LevelsManager;
        [Inject] private readonly LevelsMenuPrefabs _Prefabs;
        [Inject] private readonly PrefabInstanceCreator _InstanceCreator;

        [SerializeField] private Button _CloseButton = null;
        [SerializeField] private Transform _GridContainer = null;

        private List<UILevelsMenuGridItem> _gridItems = new List<UILevelsMenuGridItem>();

        ////////////////////////////////////////////////

        private void Start() => Init();

        private void OnEnable() =>
            _CloseButton.onClick.AddListener(OnCloseClicked);

        private void OnDisable() =>
            _CloseButton.onClick.RemoveListener(OnCloseClicked);

        ////////////////////////////////////////////////

        [Button("Init")]
        private void Init()
        {
            var existingItems = _gridItems.Count;
            var levelsCount = _LevelsManager.levelsCount;
            var itemsToReuse = Mathf.Min(existingItems, levelsCount);

            // переиспользуем уже существующие элементы
            for (var i = 0; i < itemsToReuse; i++)
                _gridItems[i].Init(i);

            // досоздаём недостающие элементы
            for (var i = itemsToReuse; i < levelsCount; i++)
                CreateGridItem(i);

            // или удалим избыточные
            for (var i = levelsCount; i < existingItems; i++)
                DestroyGridItem(levelsCount);
        }

        ////////////////////////////////////////////////

        private UILevelsMenuGridItem CreateGridItem(int levelIndex)
        {
            var item = _InstanceCreator.Instantiate(_Prefabs.levelsMenuGridItem, _GridContainer);
            item.onClick += OnItemClicked;
            item.Init(levelIndex);

            _gridItems.Add(item);
            return item;
        }

        ////////////////////////////////////////////////

        private void DestroyGridItem(int itemIndex) =>
            DestroyGridItem(_gridItems[itemIndex]);

        private void DestroyGridItem(UILevelsMenuGridItem gridItem)
        {
            gridItem.onClick -= OnItemClicked;
            _gridItems.Remove(gridItem);

            Destroy(gridItem.gameObject);
        }

        ////////////////////////////////////////////////

        private void OnItemClicked(int levelIndex)
        {
            Log($"Click {levelIndex}");
            _LevelsManager.SetCurrentLevel(levelIndex);
            _ScenesManager.LoadLevelScene();
        }

        private void OnCloseClicked()
        {
            Log("Close clicked");
            _ScenesManager.LoadPrevScene();
        }
    }
}
