﻿using System.Collections.Generic;
using Game.Core.Components;
using Game.Core.Components.UI;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Game.Scripts.LevelsMenu.UI
{
    public class UILevelsMenuGridItem : LoggedMonoBehaviour
    {
        [SerializeField] private Button _Button;
        [SerializeField] private UniversalText _Text;

        [ShowInInspector, ReadOnly] public int levelIndex { get; private set; }
        public event UnityAction<int> onClick
        {
            add => _onClickActions.Add(value);
            remove => _onClickActions.Remove(value);
        }

        private readonly HashSet<UnityAction<int>> _onClickActions = new HashSet<UnityAction<int>>();

        ////////////////////////////////////////////////

        private void OnEnable() => _Button.onClick.AddListener(OnClick);
        private void OnDisable() => _Button.onClick.RemoveListener(OnClick);

        ////////////////////////////////////////////////

        public void Init(int levelIndex)
        {
            this.levelIndex = levelIndex;
            _Text.SetText($"Level {levelIndex}");
        }

        ////////////////////////////////////////////////

        private void OnClick()
        {
            foreach (var action in _onClickActions)
                action?.Invoke(levelIndex);
        }
    }
}
