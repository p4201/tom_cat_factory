﻿using UnityEngine;
using Zenject;

namespace Game.Scripts.Components
{
    public class GameObjectsPool<T>: MemoryPool<T> where T : GameObjectController
    {
        private readonly Transform _gameContainer;
        private readonly Transform _poolContainer;

        ////////////////////////////////////////////////

        public GameObjectsPool(Transform gameContainer, Transform poolContainer)
        {
            _gameContainer = gameContainer;
            _poolContainer = poolContainer;
        }

        ////////////////////////////////////////////////

        protected override void OnCreated(T item)
        {
            item.finalized = true;
            item.gameObject.SetActive(false);
            item.transform.SetParent(_gameContainer);
        }

        protected override void OnSpawned(T item)
        {
            item.finalized = false;
            item.gameObject.SetActive(true);
            item.transform.SetParent(_gameContainer);
        }

        protected override void OnDespawned(T item)
        {
            item.transform.SetParent(_poolContainer);
            item.Reset();

            item.gameObject.SetActive(false);
            item.finalized = true;
        }
    }
}
