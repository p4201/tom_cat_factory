﻿using Game.Core.Components;
using UnityEngine.Events;

namespace Game.Scripts.Components
{
    public abstract class GameObjectsGenerator<T>: LoggedMonoBehaviour where T : GameObjectController
    {
        public abstract UnityEvent<T> onCreate { get; }
        public abstract UnityEvent<T> onFinalize { get; }

        protected abstract GameObjectsPool<T> _pool { get; }

        ////////////////////////////////////////////////

        public T Generate()
        {
            Log("Generate");
            return _pool.Spawn();
        }

        public void Finalize(T obj)
        {
            if (obj.finalized)
                return;

            Log("Finalize");
            _pool.Despawn(obj);
        }
    }
}
