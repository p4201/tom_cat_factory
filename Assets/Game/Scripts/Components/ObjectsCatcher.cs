﻿using Game.Core.Components;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Scripts.Components
{
    [RequireComponent(typeof(Collider2D))]
    public abstract class ObjectsCatcher<T> : LoggedMonoBehaviour where T : MonoBehaviour
    {
        public abstract UnityEvent<T> onCaught { get; }

        ////////////////////////////////////////////////

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.TryGetComponent<T>(out var obj) == false)
                return;

            Log("Caught");
            onCaught?.Invoke(obj);
        }
    }
}
