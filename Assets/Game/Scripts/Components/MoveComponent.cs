﻿using Game.Core.Components;
using Game.Core.Components.Timing;
using Game.Core.Interfaces;
using Game.Scripts.Definitions.Speed;
using UnityEngine;

namespace Game.Scripts.Components
{
    [RequireComponent(typeof(StopwatchComponent))]
    public class MoveComponent : LoggedMonoBehaviour, IResettable
    {
        [SerializeField] private StopwatchComponent _Stopwatch;
        [SerializeField] private Transform _Object;
        [SerializeField] private SpeedConfig _SpeedConfig;
        [SerializeField] private Vector2 _Direction;

        public bool isPaused => _Stopwatch.isPaused;
        public bool isMoving => _Stopwatch.isCounting;

        ////////////////////////////////////////////////

        private void OnEnable() =>
            _Stopwatch.onUpdate.AddListener(OnUpdate);

        private void OnDisable() =>
            _Stopwatch.onUpdate.RemoveListener(OnUpdate);

        ////////////////////////////////////////////////

        public void SetCurrentTime(float currentTime)
        {
            Log($"Set time: {currentTime}");
            _Stopwatch.SetCurrentTime(currentTime);
        }

        public void Init(Transform moveObject, SpeedConfig speedConfig, Vector2 direction)
        {
            Log("Init");

            _Object = moveObject;
            _Direction = direction;
            _SpeedConfig = speedConfig;
        }

        public void Reset()
        {
            Log("Reset");
            StopMove();
        }

        ////////////////////////////////////////////////

        [ContextMenu("Start Move")]
        public void StartMove()
        {
            Log("Start Move");
            _Stopwatch.StartCounting();
        }

        [ContextMenu("Pause Move")]
        public void PauseMove()
        {
            Log("Paused");
            _Stopwatch.PauseCounting();
        }

        [ContextMenu("Stop Move")]
        public void StopMove()
        {
            Log("Stop");
            _Stopwatch.StopCounting();
        }

        ////////////////////////////////////////////////

        private void OnUpdate(float dt)
        {
            var direction = _Direction;
            var speed = _SpeedConfig.speed.GetCurrent(_Stopwatch.currentTime);

            _Object.localPosition += (Vector3) direction * (speed * dt);
        }
    }
}
