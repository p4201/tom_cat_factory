﻿using Game.Core.Components;
using Game.Core.Interfaces;

namespace Game.Scripts.Components
{
    public class GameObjectModel : LoggedClass, IResettable
    {
        public virtual void Reset() => Log("Reset");
    }
}
