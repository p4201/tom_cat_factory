﻿using System;
using Game.Core.Components;
using Game.Core.Interfaces;

namespace Game.Scripts.Components
{
    public class GameObjectController : LoggedMonoBehaviour, IResettable
    {
        public event Action<GameObjectController> onDestroy;
        public event Action<GameObjectController> needFinalize;

        public bool finalized { get; set; }

        ////////////////////////////////////////////////

        private void OnDestroy() => onDestroy?.Invoke(this);

        ////////////////////////////////////////////////

        public virtual void Reset() => Log("Reset");

        public void CallFinalize() => needFinalize?.Invoke(this);
    }
}
