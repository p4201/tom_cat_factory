﻿using Game.Scripts.Level.Generation.Container;
using Game.Scripts.Level.Management;
using Game.Scripts.Level.Objects.Container;
using UnityEngine;

namespace Game.Scripts.Level.Installers
{
    public class ContainersInstaller
        : LeveObjectsInstaller<ContainerController, ContainersManager, ContainersGenerator, ContainersPool>
    {
        [Header("Components")]
        [SerializeField] private ContainersManager _ContainersManager = null;
        [SerializeField] private ContainersGenerator _ContainersGenerator = null;

        protected override ContainersManager _manager => _ContainersManager;
        protected override ContainersGenerator _generator => _ContainersGenerator;

        protected override GameObject _prefab => _PrefabsRoster.container;
    }
}
