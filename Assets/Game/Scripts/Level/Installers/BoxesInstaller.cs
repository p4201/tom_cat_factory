﻿using Game.Scripts.Level.Generation.Box;
using Game.Scripts.Level.Management;
using Game.Scripts.Level.Objects.Box;
using UnityEngine;

namespace Game.Scripts.Level.Installers
{
    public class BoxesInstaller : LeveObjectsInstaller<BoxController, BoxesManager, BoxesGenerator, BoxesPool>
    {
        [Header("Components")]
        [SerializeField] private BoxesManager _BoxesManager = null;
        [SerializeField] private BoxesGenerator _BoxesGenerator = null;

        protected override BoxesManager _manager => _BoxesManager;
        protected override BoxesGenerator _generator => _BoxesGenerator;

        protected override GameObject _prefab => _PrefabsRoster.box;
    }
}
