﻿using Game.Scripts.Level.Definitions;
using Game.Scripts.Level.Generation;
using Game.Scripts.Level.Management;
using Game.Scripts.Level.Objects;
using UnityEngine;
using Zenject;

namespace Game.Scripts.Level.Installers
{
    public abstract class LeveObjectsInstaller<TController, TManager, TGenerator, TPool>
        : MonoInstaller<LeveObjectsInstaller<TController, TManager, TGenerator, TPool>>
        where TController : LevelObjectController
        where TManager : LevelObjectsManager<TController>
        where TGenerator : LevelObjectsGenerator<TController>
        where TPool : LevelObjectsPool<TController>
    {
        [Inject] protected PrefabsRoster _PrefabsRoster { get; }

        [Header("Configs")]
        [SerializeField, Min(0)] private int _InitialPoolSize;

        [Header("Containers")]
        [SerializeField] private Transform _GameContainer = null;
        [SerializeField] private Transform _PoolContainer = null;

        protected abstract TManager _manager { get; }
        protected abstract TGenerator _generator { get; }

        protected abstract GameObject _prefab { get; }

        ////////////////////////////////////////////////

        public override void InstallBindings()
        {
            Container.BindMemoryPool<TController, TPool>()
                .WithInitialSize(_InitialPoolSize)
                .WithFactoryArguments(_GameContainer, _PoolContainer)
                .FromComponentInNewPrefab(_prefab);

            Container.Bind<TGenerator>().FromInstance(_generator).AsSingle();
            Container.Bind<TManager>().FromInstance(_manager).AsSingle();
        }
    }
}
