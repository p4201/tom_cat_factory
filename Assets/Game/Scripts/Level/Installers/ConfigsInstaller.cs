﻿using Game.Scripts.Level.Definitions;
using Game.Scripts.Level.Definitions.Level;
using UnityEngine;
using Zenject;

namespace Game.Scripts.Level.Installers
{
    public class ConfigsInstaller : MonoInstaller<ConfigsInstaller>
    {
        [SerializeField] private UIRewardsViewConfig _RewardsViewConfig = null;
        [SerializeField] private PhysicsConfig _PhysicsConfig = null;
        [SerializeField] private PrefabsRoster _PrefabsRoster = null;

        public override void InstallBindings()
        {
            Container.Bind<UIRewardsViewConfig>().FromInstance(_RewardsViewConfig).AsSingle();
            Container.Bind<PhysicsConfig>().FromInstance(_PhysicsConfig).AsSingle();
            Container.Bind<PrefabsRoster>().FromInstance(_PrefabsRoster).AsSingle();
        }
    }
}
