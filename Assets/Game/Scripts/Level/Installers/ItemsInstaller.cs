﻿using Game.Scripts.Level.Generation.Item;
using Game.Scripts.Level.Management;
using Game.Scripts.Level.Objects.Item;
using UnityEngine;

namespace Game.Scripts.Level.Installers
{
    public class ItemsInstaller : LeveObjectsInstaller<ItemController, ItemsManager, ItemsGenerator, ItemsPool>
    {
        [Header("Components")]
        [SerializeField] private ItemsManager _ItemsManager = null;
        [SerializeField] private ItemsGenerator _ItemsGenerator = null;

        protected override ItemsManager _manager => _ItemsManager;
        protected override ItemsGenerator _generator => _ItemsGenerator;

        protected override GameObject _prefab => _PrefabsRoster.item;
    }
}
