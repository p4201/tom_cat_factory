﻿using Game.Core.Components.Timing;
using Game.Core.Components.UI;
using Game.Core.DI;
using Game.Core.Modules;
using Game.Scripts.Level.Management;
using Game.Scripts.Level.Objects.Container;
using Game.Scripts.Level.Objects.Item;
using Game.Scripts.Level.Objects.Transporter;
using UnityEngine;
using Zenject;

namespace Game.Scripts.Level.Installers
{
    public class SceneObjectsInstaller : MonoInstaller<SceneObjectsInstaller>
    {
        [SerializeField] private GameObject _Timer = null;
        [SerializeField] private GameObject _UIManager = null;
        [SerializeField] private GameObject _GameManager = null;
        [SerializeField] private GameObject _Transporter = null;
        [SerializeField] private GameObject _ItemsCatcher = null;
        [SerializeField] private GameObject _ContainersCatcher = null;

        ////////////////////////////////////////////////

        public override void InstallBindings()
        {
            Container.Bind<PrefabInstanceCreator>().FromInstance(new PrefabInstanceCreator(Container)).AsSingle();
            Container.Bind<UIManager>().FromComponentOn(_UIManager).AsSingle();
            Container.Bind<TimerComponent>().FromComponentOn(_Timer).AsSingle();
            Container.Bind<TransporterController>().FromComponentOn(_Transporter).AsSingle();
            Container.Bind<ItemsCatcher>().FromComponentOn(_ItemsCatcher).AsSingle();
            Container.Bind<ContainersCatcher>().FromComponentOn(_ContainersCatcher).AsSingle();

            Container.Bind<TimeScaleManager>().AsSingle();
            Container.Bind<ItemHitsRaycaster>().AsSingle();
            Container.Bind<ContainerQueueManager>().AsSingle();
            Container.Bind<GameManager>().FromComponentOn(_GameManager).AsSingle();
            Container.Bind<LevelManager>().FromComponentOn(_GameManager).AsSingle();
        }
    }
}
