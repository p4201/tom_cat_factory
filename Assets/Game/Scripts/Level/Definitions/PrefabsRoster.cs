﻿using Game.Scripts.Definitions;
using UnityEngine;

namespace Game.Scripts.Level.Definitions
{
    [CreateAssetMenu(fileName = nameof(PrefabsRoster), menuName = DefinitionsHelper.Level.PATH + nameof(PrefabsRoster))]
    public class PrefabsRoster : ScriptableObject
    {
        [SerializeField] private GameObject _Box = null;
        [SerializeField] private GameObject _Item = null;
        [SerializeField] private GameObject _Container = null;

        public GameObject box => _Box;
        public GameObject item => _Item;
        public GameObject container => _Container;
    }
}
