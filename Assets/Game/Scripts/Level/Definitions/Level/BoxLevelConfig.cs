﻿using System;
using UnityEngine;

namespace Game.Scripts.Level.Definitions.Level
{
    // можно сделать список с несколькими целями.
    // а ещё лучше сделать ITarget с различными реализациями,
    // одна из которых будет SingleTarget как раз с полями _TargetItem и _Capacity.
    [Serializable]
    public class BoxLevelConfig
    {
        [SerializeField] private ItemConfig _TargetItem;
        [SerializeField,Min(0)] private int _Capacity;
        [SerializeField] private Vector2 _Position;

        public ItemConfig targetItem => _TargetItem;
        public int capacity => _Capacity;
        public Vector2 position => _Position;

        ////////////////////////////////////////////////

        private BoxLevelConfig(ItemConfig targetItem = null, int capacity = 0)
            : this(targetItem, capacity, Vector2.zero) { }
        private BoxLevelConfig(ItemConfig targetItem, int capacity, Vector2 position)
        {
            _TargetItem = targetItem;
            _Capacity = capacity;
            _Position = position;
        }
    }
}
