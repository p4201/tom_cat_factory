﻿using Game.Scripts.Definitions;
using UnityEngine;

namespace Game.Scripts.Level.Definitions.Level
{
    [CreateAssetMenu(fileName = nameof(ItemConfig), menuName = DefinitionsHelper.PATH + nameof(ItemConfig))]
    public class ItemConfig : ScriptableObject
    {
        [SerializeField] private string _Id = "";
        [SerializeField] private Sprite _Icon = null;
        [SerializeField] private Sprite _Sprite = null;

        public string id => _Id;
        public Sprite icon => _Icon;
        public Sprite sprite => _Sprite;
    }
}
