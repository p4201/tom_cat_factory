﻿using Game.Scripts.Definitions;
using Game.Scripts.Level.UI.Objects;
using UnityEngine;

namespace Game.Scripts.Level.Definitions.Level
{
    [CreateAssetMenu(fileName = nameof(UIRewardsViewConfig), menuName = DefinitionsHelper.UI_PATH + nameof(UIRewardsViewConfig))]
    public class UIRewardsViewConfig : ScriptableObject
    {
        [SerializeField] private Sprite _ActiveStarIcon = null;
        [SerializeField] private Sprite _InactiveStarIcon = null;
        [SerializeField] private UIRewardView _RewardPrefab = null;

        public Sprite activeStarIcon => _ActiveStarIcon;
        public Sprite inactiveStarIcon => _InactiveStarIcon;
        public UIRewardView rewardPrefab => _RewardPrefab;

        ////////////////////////////////////////////////

        public Sprite GetStarIcon(bool active) =>
            active ? activeStarIcon : inactiveStarIcon;
    }
}
