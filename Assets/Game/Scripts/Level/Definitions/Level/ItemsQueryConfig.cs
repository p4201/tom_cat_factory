﻿using System;
using System.Collections.Generic;
using Game.Core.Extensions;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Level.Definitions.Level
{
    [Serializable, HideLabel]
    public class ItemsQueryConfig
    {
        [SerializeField] private ItemInQueryConfig[] _Items;

        public int count => _Items.SafeLength();
        public IReadOnlyList<ItemInQueryConfig> items => _Items;

        ////////////////////////////////////////////////

        private ItemsQueryConfig(ItemInQueryConfig[] items = null)
        {
            _Items = items ?? new ItemInQueryConfig[0];
        }

        ////////////////////////////////////////////////

        public ItemInQueryConfig GetConfig(int index) =>
            IsIndexValid(index) ? _Items[index] : null;

        public bool TryGetConfig(int index, out ItemInQueryConfig config) =>
            (config = GetConfig(index)) != null;

        private bool IsIndexValid(int index) =>
            index >= 0 && index < count;
    }

    [Serializable]
    public class ItemInQueryConfig
    {
        [SerializeField] private ItemConfig _Item;
        [SerializeField] private float _DistanceFromStart;

        public ItemConfig item => _Item;
        public float distanceFromStart => _DistanceFromStart;

        ////////////////////////////////////////////////

        private ItemInQueryConfig(ItemConfig item = null, float distanceFromStart = 0)
        {
            _Item = item;
            _DistanceFromStart = distanceFromStart;
        }
    }
}
