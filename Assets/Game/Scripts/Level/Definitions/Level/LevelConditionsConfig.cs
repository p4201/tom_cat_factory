﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Core.Interfaces;
using Game.Scripts.Definitions.Valuables;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Level.Definitions.Level
{
    [Serializable, HideLabel, FoldoutGroup("Conditions")]
    public class LevelConditionsConfig : IValidatable
    {
        [SerializeField, Min(0)] private int _TimeLimitSec = 0;

        [SerializeField, ValidateInput(nameof(_isArrayValid))]
        private RewardsForBoxes[] _RewardsForBoxes = new RewardsForBoxes[0];

        private bool _isArrayValid = true;
        private bool _isSorted = false;

        public int timeLimitSec => _TimeLimitSec;

        ////////////////////////////////////////////////

        public LevelConditionsConfig(int timeLimitSec = 0) =>
            _TimeLimitSec = timeLimitSec;

        ////////////////////////////////////////////////

        public void Validate()
        {
            Sort();
            _isArrayValid = AreDataInRightOrder();
        }

        private bool AreDataInRightOrder()
        {
            for (var i = 0; i < _RewardsForBoxes.Length - 1; i++)
            {
                var currentBoxes = _RewardsForBoxes[i].boxes;
                var nextBoxes = _RewardsForBoxes[i + 1].boxes;

                if (currentBoxes >= nextBoxes && nextBoxes != 0)
                    return false;
            }

            return true;
        }

        private void Sort()
        {
            if (_isSorted) return;

            _isSorted = true;
            _RewardsForBoxes = _RewardsForBoxes.OrderBy(e => e.boxes).ToArray();
        }

        ////////////////////////////////////////////////

        public IReadOnlyList<CountableValuableConfig> GetRewardsForBoxes(int boxes)
        {
            Sort();

            var targetIndex = -1;
            for (var i = 0; i < _RewardsForBoxes.Length; i++)
            {
                if (boxes < _RewardsForBoxes[i].boxes)
                    break;

                targetIndex = i;
            }

            return targetIndex >= 0 ? _RewardsForBoxes[targetIndex].rewards : new CountableValuableConfig[0];
        }
    }

    [Serializable]
    public struct RewardsForBoxes
    {
        [SerializeField, Min(0), Tooltip("Сколько коробок требуется завершить для получения указанной награды")]
        private int _Boxes;

        [SerializeField, Tooltip("Награда за указанное количество коробок")]
        private CountableValuableConfig[] _Rewards;


        public int boxes => _Boxes;
        public IReadOnlyList<CountableValuableConfig> rewards => _Rewards;

        ////////////////////////////////////////////////

        private RewardsForBoxes(int boxes, CountableValuableConfig[] rewards)
        {
            _Boxes = boxes;
            _Rewards = rewards;
        }
    }
}
