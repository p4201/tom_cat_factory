﻿using System;
using System.Collections.Generic;
using Game.Core.Extensions;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Level.Definitions.Level
{
    [Serializable, HideLabel]
    public class BoxesSetConfig
    {
        [SerializeField] private BoxLevelConfig[] _Boxes;

        public int count => _Boxes.SafeLength();
        public IReadOnlyList<BoxLevelConfig> boxes => _Boxes;

        ////////////////////////////////////////////////

        private BoxesSetConfig(BoxLevelConfig[] configs) =>
            _Boxes = configs;

        private BoxesSetConfig() : this(new BoxLevelConfig[0])
        { }

        ////////////////////////////////////////////////

        public BoxLevelConfig GetBoxConfig(int index) =>
            IsIndexValid(index) ? _Boxes[index] : null;

        public bool TryGetBoxConfig(int index, out BoxLevelConfig config) =>
            (config = GetBoxConfig(index)) != null;

        private bool IsIndexValid(int index) =>
            index >= 0 && index < count;
    }
}
