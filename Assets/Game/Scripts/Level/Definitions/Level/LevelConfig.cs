﻿using Game.Scripts.Definitions;
using UnityEngine;

namespace Game.Scripts.Level.Definitions.Level
{
    [CreateAssetMenu(fileName = nameof(LevelConfig), menuName = DefinitionsHelper.Level.PATH + nameof(LevelConfig))]
    public class LevelConfig : ScriptableObject
    {
        [SerializeField] private LevelConditionsConfig _Conditions = null;
        [SerializeField] private TransporterConfig _TransporterConfig = null;
        [SerializeField] private ItemsQueryConfig _Items = null;
        [SerializeField] private BoxesSetConfig _Boxes = null;
        [SerializeField] private FallingConfig _Falling = null;

        public LevelConditionsConfig conditions => _Conditions;
        public TransporterConfig transporter => _TransporterConfig;
        public ItemsQueryConfig items => _Items;
        public BoxesSetConfig boxes => _Boxes;
        public FallingConfig falling => _Falling;

        ////////////////////////////////////////////////

        private void OnValidate()
        {
            _Conditions.Validate();
            _TransporterConfig.Validate();
            _Falling.Validate();
        }
    }
}
