﻿using System;
using Game.Core.Interfaces;
using Game.Scripts.Definitions.Direction;
using Game.Scripts.Definitions.Speed;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Game.Scripts.Level.Definitions.Level
{
    [Serializable, HideLabel, FoldoutGroup("Transporter")]
    public class TransporterConfig : IValidatable
    {
        [SerializeField] private SpeedConfig _Speed;
        [SerializeField] private DirectionConfig _Direction;

        public SpeedConfig speed => _Speed;
        public Vector2 direction => _Direction.direction;

        ////////////////////////////////////////////////

        private TransporterConfig(SpeedConfig speedConfig = null, DirectionConfig directionConfig = null)
        {
            _Speed = speedConfig;
            _Direction = directionConfig;
        }

        ////////////////////////////////////////////////

        public void Validate()
        {
            _Direction.Validate();
        }
    }
}
