﻿using Game.Scripts.Definitions;
using UnityEngine;

namespace Game.Scripts.Level.Definitions
{
    [CreateAssetMenu(fileName = nameof(PhysicsConfig), menuName = DefinitionsHelper.Level.PATH + nameof(PhysicsConfig))]
    public class PhysicsConfig : ScriptableObject
    {
        [SerializeField, Min(0)] private int _MaxRaycastHits = 0;
        [SerializeField] private ContactFilter2D _BoxContactFilter = new ContactFilter2D();

        public int maxRaycastHits => _MaxRaycastHits;
        public ContactFilter2D boxContactFilter => _BoxContactFilter;
    }
}
