﻿using Game.Scripts.Components;
using Game.Scripts.Level.Objects;
using UnityEngine;

namespace Game.Scripts.Level.Generation
{
    public class LevelObjectsPool<T> : GameObjectsPool<T> where T : LevelObjectController
    {
        public LevelObjectsPool(Transform gameContainer, Transform poolContainer) : base(gameContainer, poolContainer)
        { }
    }
}
