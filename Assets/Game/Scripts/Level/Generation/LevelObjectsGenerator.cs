﻿using Game.Scripts.Components;
using Game.Scripts.Level.Objects;

namespace Game.Scripts.Level.Generation
{
    public abstract class LevelObjectsGenerator<T> : GameObjectsGenerator<T> where T : LevelObjectController
    {
    }
}
