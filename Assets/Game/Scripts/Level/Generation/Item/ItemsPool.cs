﻿using Game.Scripts.Level.Objects.Item;
using UnityEngine;

namespace Game.Scripts.Level.Generation.Item
{
    public class ItemsPool : LevelObjectsPool<ItemController>
    {
        public ItemsPool(Transform gameContainer, Transform poolContainer) : base(gameContainer, poolContainer)
        { }
    }
}
