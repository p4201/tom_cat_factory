﻿using Game.Scripts.Components;
using Game.Scripts.Level.Extensions;
using Game.Scripts.Level.Objects.Item;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Game.Scripts.Level.Generation.Item
{
    public class ItemsGenerator : LevelObjectsGenerator<ItemController>
    {
        [Inject] private readonly ItemsPool _Pool;

        [Header("Events")]
        [SerializeField] private UnityEventItemController _OnCreate = new UnityEventItemController();
        [SerializeField] private UnityEventItemController _OnFinalize = new UnityEventItemController();

        public override UnityEvent<ItemController> onCreate => _OnCreate;
        public override UnityEvent<ItemController> onFinalize => _OnFinalize;

        protected override GameObjectsPool<ItemController> _pool => _Pool;
    }
}
