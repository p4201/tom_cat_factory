﻿using Game.Scripts.Components;
using Game.Scripts.Level.Extensions;
using Game.Scripts.Level.Objects.Container;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Game.Scripts.Level.Generation.Container
{
    public class ContainersGenerator : LevelObjectsGenerator<ContainerController>
    {
        [Inject] private readonly ContainersPool _Pool;

        [Header("Events")]
        [SerializeField] private UnityEventContainerController _OnCreate = new UnityEventContainerController();
        [SerializeField] private UnityEventContainerController _OnFinalize = new UnityEventContainerController();

        public override UnityEvent<ContainerController> onCreate => _OnCreate;
        public override UnityEvent<ContainerController> onFinalize => _OnFinalize;

        protected override GameObjectsPool<ContainerController> _pool => _Pool;
    }
}
