﻿using Game.Scripts.Level.Objects.Container;
using UnityEngine;

namespace Game.Scripts.Level.Generation.Container
{
    public class ContainersPool : LevelObjectsPool<ContainerController>
    {
        public ContainersPool(Transform gameContainer, Transform poolContainer) : base(gameContainer, poolContainer)
        { }
    }
}
