﻿using Game.Scripts.Level.Objects.Box;
using UnityEngine;

namespace Game.Scripts.Level.Generation.Box
{
    public class BoxesPool : LevelObjectsPool<BoxController>
    {
        public BoxesPool(Transform gameContainer, Transform poolContainer) : base(gameContainer, poolContainer)
        { }
    }
}
