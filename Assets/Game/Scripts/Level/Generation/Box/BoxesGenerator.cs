﻿using Game.Scripts.Components;
using Game.Scripts.Level.Extensions;
using Game.Scripts.Level.Objects.Box;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Game.Scripts.Level.Generation.Box
{
    public class BoxesGenerator : LevelObjectsGenerator<BoxController>
    {
        [Inject] private readonly BoxesPool _Pool;

        [Header("Events")]
        [SerializeField] private UnityEventBoxController _OnCreate = new UnityEventBoxController();
        [SerializeField] private UnityEventBoxController _OnFinalize = new UnityEventBoxController();

        public override UnityEvent<BoxController> onCreate => _OnCreate;
        public override UnityEvent<BoxController> onFinalize => _OnFinalize;

        protected override GameObjectsPool<BoxController> _pool => _Pool;
    }
}
