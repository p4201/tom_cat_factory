﻿using System.Collections.Generic;
using Game.Scripts.Level.Definitions.Level;
using Game.Scripts.Level.Generation;
using Game.Scripts.Level.Generation.Item;
using Game.Scripts.Level.Objects.Item;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Game.Scripts.Level.Management
{
    public class ItemsManager : LevelObjectsManager<ItemController>
    {
        [Inject] private readonly ItemsGenerator _Generator;

        [Header("Events")]
        [SerializeField] private UnityEvent _OnCaught = new UnityEvent();

        protected override LevelObjectsGenerator<ItemController> _generator => _Generator;
        public IReadOnlyList<ItemController> items => _objects;
        public UnityEvent onCaught => _OnCaught;

        ////////////////////////////////////////////////

        public void Init()
        {
            Log("Init");
            FinalizeAll();
        }

        ////////////////////////////////////////////////

        public ItemController CreateItem(ItemConfig itemConfig, FallingConfig fallingConfig)
        {
            var item = CreateObject();
            item.onCaught.AddListener(CallOnCaught);
            item.Init(itemConfig, fallingConfig);

            return item;
        }

        public void FinalizeItem(ItemController item) => FinalizeObject(item);

        ////////////////////////////////////////////////

        protected override void OnFinalizeObject(ItemController item) =>
            item.onCaught.RemoveListener(CallOnCaught);

        ////////////////////////////////////////////////

        private void CallOnCaught() => _OnCaught?.Invoke();
    }
}
