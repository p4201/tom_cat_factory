﻿using System.Linq;
using Game.Core.Components;
using Game.Core.Components.Timing;
using Game.Core.Extensions;
using Game.Core.Interfaces;
using Game.Scripts.Level.Definitions;
using Game.Scripts.Level.Definitions.Level;
using Game.Scripts.Level.Objects.Box;
using Game.Scripts.Level.Objects.Container;
using Game.Scripts.Level.Objects.Item;
using Game.Scripts.Level.Objects.Transporter;
using UnityEngine;
using Zenject;

namespace Game.Scripts.Level.Management
{
    public class GameManager : LoggedMonoBehaviour, IResettable
    {
        [Inject] private readonly ItemHitsRaycaster _HitRaycaster;
        [Inject] private readonly ContainerQueueManager _ContainerQueueManager;

        [Inject] private readonly TimerComponent _Timer;
        [Inject] private readonly ItemsManager _ItemsManager;
        [Inject] private readonly ItemsCatcher _ItemsCatcher;
        [Inject] private readonly BoxesManager _BoxesManager;
        [Inject] private readonly PhysicsConfig _PhysicsConfig;
        [Inject] private readonly ContainersManager _ContainersManager;
        [Inject] private readonly ContainersCatcher _ContainersCatcher;
        [Inject] private readonly TransporterController _TransporterController;

        [Header("Events")]
        [SerializeField] private UnityEventInt _OnFinished = new UnityEventInt();

        private LevelConfig _config;

        public UnityEventInt onFinished => _OnFinished;

        ////////////////////////////////////////////////

        private void OnDestroy()
        {
            Unsubscribe();
        }

        ////////////////////////////////////////////////

        public void Init(LevelConfig config)
        {
            Log("Init");
            _config = config;

            _ItemsManager.Init();
            _BoxesManager.Init(config.boxes);
            _HitRaycaster.Init(_PhysicsConfig);
            _TransporterController.Init(config.transporter);

            _Timer.StopTimer();
            _Timer.SetCapacity(config.conditions.timeLimitSec);

            _ContainersManager.Init(config.transporter.direction);
            _ContainerQueueManager.Init(config.items.items.Select(i => i.distanceFromStart).ToArray());

            Unsubscribe();
            Subscribe();
        }

        public void Reset()
        {
            Log("Reset");
            _ItemsManager.Reset();
            _BoxesManager.Reset();
            _ContainersManager.Reset();
            _TransporterController.Reset();

            _Timer.Reset();
            _HitRaycaster.Reset();
            _ContainerQueueManager.Reset();
        }

        ////////////////////////////////////////////////

        [ContextMenu("Start Game")]
        public void StartGame()
        {
            Log("Start Game");
            _Timer.StartTimer();
            _TransporterController.StartTransport();
        }

        [ContextMenu("Finish Game")]
        public void FinishGame()
        {
            _Timer.StopTimer();
            _TransporterController.StopTransport();

            var boxes = _BoxesManager.completedBoxesCount;
            Log($"Finished game with {boxes} boxes");

            _OnFinished?.Invoke(boxes);
        }

        ////////////////////////////////////////////////

        private void Subscribe()
        {
            Log("Subscribe");

            _ItemsManager.onFinalize += OnItemFinalize;
            _ContainerQueueManager.onChangeIndex += OnChangeIndex;

            _Timer.onFinish.AddListener(FinishGame);
            _ItemsCatcher.onCaught.AddListener(OnCaughtItem);
            _ContainersCatcher.onCaught.AddListener(OnCaughtContainer);
            _TransporterController.onTransporting.AddListener(OnTransporting);
            _BoxesManager.onAllCompleted.AddListener(FinishGame);
            _BoxesManager.onClick.AddListener(OnBoxClick);
        }

        private void Unsubscribe()
        {
            Log("Unsubscribe");

            _ItemsManager.onFinalize -= OnItemFinalize;
            _ContainerQueueManager.onChangeIndex -= OnChangeIndex;

            _Timer.onFinish.RemoveListener(FinishGame);
            _ItemsCatcher.onCaught.AddListener(OnCaughtItem);
            _ContainersCatcher.onCaught.RemoveListener(OnCaughtContainer);
            _TransporterController.onTransporting.RemoveListener(OnTransporting);
            _BoxesManager.onAllCompleted.RemoveListener(FinishGame);
            _BoxesManager.onClick.RemoveListener(OnBoxClick);
        }

        ////////////////////////////////////////////////

        private void OnTransporting(Vector2 diffPosition)
        {
            _ContainerQueueManager.SetDistance(_TransporterController.distance);
            _ContainersManager.MoveContainers(diffPosition);
        }

        private void OnChangeIndex(int index, float distanceOffset)
        {
            Log("Change Index");
            var fallingConfig = _config.falling;
            var itemConfig = _config.items.GetConfig(index).item;
            var item = _ItemsManager.CreateItem(itemConfig, fallingConfig);
            var container = _ContainersManager.CreateContainer(distanceOffset);

            container.SetItem(item);
        }

        private void OnCaughtContainer(ContainerController container)
        {
            Log("Caught Container");

            if (!container.isFree)
            {
                var item = container.item;
                container.ReleaseItem();

                _ItemsManager.FinalizeItem(item);
            }

            _ContainersManager.FinalizeContainer(container);
        }

        private void OnCaughtItem(ItemController item)
        {
            Log("Item caught");
            _ItemsManager.FinalizeItem(item);
        }

        private void OnItemFinalize()
        {
            // объекты кончились и больше не предвидется
            if (_ContainerQueueManager.isCompleted && !_ItemsManager.hasObjects)
                FinishGame();
        }

        private void OnBoxClick(BoxController box)
        {
            Log("Box Click");
            var direction = -_config.falling.direction;
            var boxPosition = box.transform.position;
            var hits = _HitRaycaster.GetHits(boxPosition, direction);

            foreach (var hit in hits)
                hit.collider?.GetComponent<ItemController>()?.Fall();
        }
    }
}
