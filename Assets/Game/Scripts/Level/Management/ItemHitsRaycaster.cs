﻿using System.Collections.Generic;
using Game.Core.Components;
using Game.Core.Interfaces;
using Game.Scripts.Definitions;
using Game.Scripts.Level.Definitions;
using UnityEngine;

namespace Game.Scripts.Level.Management
{
    public class ItemHitsRaycaster : LoggedClass, IResettable
    {
        private PhysicsConfig _config;
        private RaycastHit2D[] _hits;

        ////////////////////////////////////////////////

        public void Init(PhysicsConfig config)
        {
            Log("Init");
            _config = config;
            _hits = new RaycastHit2D[config.maxRaycastHits];
        }

        public void Reset()
        {
            Log("Reset");
        }

        ////////////////////////////////////////////////

        public IReadOnlyList<RaycastHit2D> GetHits(Vector2 origin, Vector2 direction)
        {
            Physics2D.Raycast(origin, direction, _config.boxContactFilter, _hits);
            return _hits;
        }
    }
}
