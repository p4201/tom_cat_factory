﻿using System;
using System.Collections.Generic;
using Game.Core.Components;
using Game.Core.Interfaces;
using Game.Scripts.Components;
using Game.Scripts.Level.Generation;
using Game.Scripts.Level.Objects;
using UnityEngine;

namespace Game.Scripts.Level.Management
{
    public abstract class LevelObjectsManager<T> : LoggedMonoBehaviour, IResettable where T : LevelObjectController
    {
        public event Action onFinalize;
        public bool hasObjects => _objects.Count > 0;

        protected readonly List<T> _objects = new List<T>();
        protected abstract LevelObjectsGenerator<T> _generator { get; }

        ////////////////////////////////////////////////

        public virtual void Reset()
        {
            Log("Reset");
            FinalizeAll();
        }

        ////////////////////////////////////////////////

        protected T CreateObject()
        {
            Log("Create");

            var obj = _generator.Generate();
            obj.needFinalize += FinalizeObject;
            _objects.Add(obj);

            return obj;
        }

        protected T CreateObject(Vector2 position)
        {
            var obj = CreateObject();
            obj.gameObject.transform.localPosition = position;

            return obj;
        }

        protected void FinalizeAll()
        {
            Log("Finalize All");
            while(_objects.Count > 0)
                FinalizeObject(_objects[0]);
        }

        protected void FinalizeObject(T obj)
        {
            Log("Finalize");

            OnFinalizeObject(obj);
            obj.needFinalize -= FinalizeObject;

            _objects.Remove(obj);
            _generator.Finalize(obj);

            onFinalize?.Invoke();
        }

        protected virtual void OnFinalizeObject(T obj) { }

        ////////////////////////////////////////////////

        private void FinalizeObject(GameObjectController obj) =>
            FinalizeObject((T) obj);
    }
}
