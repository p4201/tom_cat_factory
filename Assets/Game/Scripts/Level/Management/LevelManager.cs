﻿using Game.Core.Components;
using Game.Core.Components.UI;
using Game.Core.Components.UI.Screens;
using Game.Core.Interfaces;
using Game.Core.Modules;
using Game.Scripts.Level.Definitions.Level;
using Game.Scripts.Level.UI.Views;
using Game.Scripts.Management;
using Sirenix.OdinInspector;
using Zenject;

namespace Game.Scripts.Level.Management
{
    public class LevelManager : LoggedMonoBehaviour, IResettable
    {
        [Inject] private readonly UIManager _UIManager;
        [Inject] private readonly GameManager _GameManager;
        [Inject] private readonly LevelsManager _LevelsManager;
        [Inject] private readonly ScenesManager _ScenesManager;
        [Inject] private readonly StorageManager _StorageManager;
        [Inject] private readonly TimeScaleManager _TimeScaleManager;

        [ShowInInspector] private int _currentLevelIndex => _LevelsManager?.currentIndex ?? 0;

        ////////////////////////////////////////////////

        private void OnEnable()
        {
            _GameManager.onFinished.AddListener(OnGameFinished);
        }

        private void OnDisable()
        {
            _GameManager.onFinished.RemoveListener(OnGameFinished);
        }

        private void Start() => StartLevel();

        ////////////////////////////////////////////////

        public void Reset()
        {
            _TimeScaleManager.Reset();
            _GameManager.Reset();
        }

        ////////////////////////////////////////////////

        public void StartLevel(LevelConfig level)
        {
            Log($"Start Level {level.name}");
            _LevelsManager.SetCurrentLevel(level);
            _UIManager.Open(ScreenType.LevelGame);

            _TimeScaleManager.Reset();
            _GameManager.Init(level);
            _GameManager.StartGame();
        }

        [Button("Start Level")]
        public void StartLevel() =>
            StartLevel(_LevelsManager.currentLevel);

        [Button("Stop Level")]
        public void StopLevel()
        {
            Log("Stop Level");
            _GameManager.FinishGame();
        }

        [Button("Restart Level")]
        public void RestartLevel()
        {
            Log("Restart Level");
            Reset();
            StartLevel();
        }

        [Button("Start Next Level")]
        public void StartNextLevel() =>
            StartLevel(_LevelsManager.nextLevel);

        [Button("Start Prev Level")]
        public void StartPrevLevel() =>
            StartLevel(_LevelsManager.prevLevel);

        [Button("Pause Level")]
        public void PauseLevel()
        {
            Log("Pause Level");
            _TimeScaleManager.Stop();
            _UIManager.Open(ScreenType.LevelPause);
        }

        [Button("Unpause Level")]
        public void UnpauseLevel()
        {
            Log("Unpause Level");
            _TimeScaleManager.Start();
            _UIManager.Open(ScreenType.LevelGame);
        }

        [Button("Quit Level")]
        public void QuitLevel()
        {
            Log("Quit Level");

            _ScenesManager.LoadPrevScene();
//             Application.Quit();
// #if UNITY_EDITOR
//             UnityEditor.EditorApplication.isPlaying = false;
// #endif
        }

        ////////////////////////////////////////////////

        private void OnGameFinished(int boxesForLevel)
        {
            if (boxesForLevel > 0) CompleteLevel(boxesForLevel);
            else FailLevel();
        }

        private void CompleteLevel(int boxesForLevel)
        {
            Log($"Complete Level with {boxesForLevel} boxes");

            var rewards = _LevelsManager.currentLevel.conditions
                          .GetRewardsForBoxes(boxesForLevel);

            _UIManager.Open(ScreenType.LevelComplete)
                      .GetComponent<UILevelCompleteScreen>()
                      .SetRewards(rewards);

            _StorageManager.Add(rewards);
            _LevelsManager.SetNextLevel();

            _StorageManager.Save();
            _LevelsManager.Save();
        }

        private void FailLevel()
        {
            Log("Fail Level");
            _UIManager.Open(ScreenType.LevelFail);
        }
    }
}
