﻿using Game.Core.Components;
using Game.Core.Extensions;
using Game.Core.Interfaces;

namespace Game.Scripts.Level.Management
{
    public class ContainerQueueManager : LoggedClass, IResettable
    {
        public delegate void IndexChanged(int newIndex, float distanceOffset);

        public float currentDistance { get; private set; }
        public float[] distances { get; private set; }
        public int currentIndex { get; private set; }
        public bool isCompleted { get; private set; }

        public event IndexChanged onChangeIndex;

        ////////////////////////////////////////////////

        public void Init(float[] distances)
        {
            Log("Init");
            ResetCounting();
            this.distances = distances;
        }

        public void Reset()
        {
            Log("Reset");
            ResetCounting();
        }

        private void ResetCounting()
        {
            isCompleted = false;
            currentDistance = 0;
            currentIndex = -1;
        }

        ////////////////////////////////////////////////

        public void SetDistance(float distance)
        {
            if (distances.IsNullOrEmpty())
                return;

            var index = currentIndex;
            var maxIndex = distances.Length - 1;

            currentDistance = distance;

            // находим последний преодолённый шаг с учётом, что
            // проинициализировали стартовый индекс как -1
            for (var i = currentIndex; i <= maxIndex; i++)
            {
                if (i < 0) continue;
                if (distance <= distances[i]) break;
                index = i;
            }

            isCompleted = index >= maxIndex;

            // ничего не изменилось
            if (index == currentIndex)
                return;

            currentIndex = index;
            currentDistance = distances[currentIndex];
            var distanceOffset = distance - currentDistance;

            onChangeIndex?.Invoke(index, distanceOffset);
        }
    }
}
