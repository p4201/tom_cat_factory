﻿using System.Collections.Generic;
using Game.Scripts.Level.Generation;
using Game.Scripts.Level.Generation.Container;
using Game.Scripts.Level.Objects.Container;
using UnityEngine;
using Zenject;

namespace Game.Scripts.Level.Management
{
    public class ContainersManager : LevelObjectsManager<ContainerController>
    {
        [Inject] private readonly ContainersGenerator _Generator;

        [Header("Components")]
        [SerializeField] private Transform _CreatePivot = null;


        private Vector2 _direction;

        protected override LevelObjectsGenerator<ContainerController> _generator => _Generator;

        public IReadOnlyList<ContainerController> containers => _objects;

        ////////////////////////////////////////////////

        public void Init(Vector2 direction)
        {
            Log("Init");
            FinalizeAll();

            _direction = direction;
        }

        ////////////////////////////////////////////////

        public ContainerController CreateContainer(float distanceOffset) =>
            CreateObject((Vector2) _CreatePivot.position + _direction * distanceOffset);

        public void FinalizeContainer(ContainerController container) =>
            FinalizeObject(container);

        ////////////////////////////////////////////////

        public void MoveContainers(Vector2 diffPosition)
        {
            foreach (var container in _objects)
                container.Move(diffPosition);
        }
    }
}
