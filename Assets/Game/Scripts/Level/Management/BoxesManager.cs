﻿using System.Collections.Generic;
using Game.Scripts.Level.Definitions.Level;
using Game.Scripts.Level.Extensions;
using Game.Scripts.Level.Generation;
using Game.Scripts.Level.Generation.Box;
using Game.Scripts.Level.Objects.Box;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Game.Scripts.Level.Management
{
    public class BoxesManager : LevelObjectsManager<BoxController>
    {
        [Inject] private readonly BoxesGenerator _Generator;

        [Header("Events")]
        [SerializeField] private UnityEvent _OnAllCompleted = new UnityEvent();
        [SerializeField] private UnityEventBoxController _OnClick = new UnityEventBoxController();

        protected override LevelObjectsGenerator<BoxController> _generator => _Generator;

        public IReadOnlyList<BoxController> boxes => _objects;
        public int completedBoxesCount => GetCountOfCompletedBoxes();
        public UnityEventBoxController onClick => _OnClick;
        public UnityEvent onAllCompleted => _OnAllCompleted;

        ////////////////////////////////////////////////

        public void Init(BoxesSetConfig setConfig)
        {
            Log("Init");
            FinalizeAll();

            foreach (var boxConfig in setConfig.boxes)
                CreateBox(boxConfig.position, boxConfig.targetItem, boxConfig.capacity);
        }

        public override void Reset()
        {
            Log("Reset");

            foreach (var box in _objects)
                box.Reset();
        }

        ////////////////////////////////////////////////

        private BoxController CreateBox(Vector2 position, ItemConfig targetItem, int capacity)
        {
            var box = CreateBox(position);
            box.Init(targetItem, capacity);

            box.onClick.AddListener(OnBoxClick);
            box.onComplete.AddListener(OnBoxComplete);

            return box;
        }

        private BoxController CreateBox(Vector2 position) => CreateObject(position);

        private void FinalizeBox(BoxController box) => FinalizeObject(box);

        ////////////////////////////////////////////////

        protected override void OnFinalizeObject(BoxController box)
        {
            box.onClick.RemoveListener(OnBoxClick);
            box.onComplete.RemoveListener(OnBoxComplete);
        }

        ////////////////////////////////////////////////

        private void OnBoxClick(BoxController box) => _OnClick?.Invoke(box);

        private void OnBoxComplete()
        {
            if (!IsAllBoxesComplete())
                return;

            Log("All Completed!");
            _OnAllCompleted?.Invoke();
        }

        ////////////////////////////////////////////////

        private int GetCountOfCompletedBoxes()
        {
            var counter = 0;

            for (var i = 0; i < _objects.Count; i++)
                if (_objects[i].isCompleted)
                    counter++;

            return counter;
        }

        private bool IsAllBoxesComplete()
        {
            for (var i = 0; i < _objects.Count; i++)
                if (!_objects[i].isCompleted)
                    return false;

            return true;
        }
    }
}
