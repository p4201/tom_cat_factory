﻿using System;
using Game.Scripts.Level.Objects.Box;
using Game.Scripts.Level.Objects.Container;
using Game.Scripts.Level.Objects.Item;
using UnityEngine.Events;

namespace Game.Scripts.Level.Extensions
{
    [Serializable] public class UnityEventBoxController : UnityEvent<BoxController> { }
    [Serializable] public class UnityEventItemController : UnityEvent<ItemController> { }
    [Serializable] public class UnityEventContainerController : UnityEvent<ContainerController> { }
}
