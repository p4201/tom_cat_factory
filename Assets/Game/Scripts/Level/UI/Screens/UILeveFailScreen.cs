﻿using Game.Core.Components.UI.Screens;
using Game.Scripts.Level.Management;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.Scripts.Level.UI.Views
{
    [RequireComponent(typeof(ScreenController))]
    public class UILeveFailScreen : ScreenBehaviour
    {
        [Inject] private readonly LevelManager _LevelManager;

        [Header("Components")]
        [SerializeField] private Button _RestartButton = null;
        [SerializeField] private Button _QuitButton = null;

        ////////////////////////////////////////////////

        private void OnEnable()
        {
            _RestartButton.onClick.AddListener(OnClickRestart);
            _QuitButton.onClick.AddListener(OnClickQuit);
        }

        private void OnDisable()
        {
            _RestartButton.onClick.RemoveListener(OnClickRestart);
            _QuitButton.onClick.RemoveListener(OnClickQuit);
        }

        ////////////////////////////////////////////////

        private void OnClickRestart()
        {
            Log("Restart Click");
            _LevelManager.RestartLevel();
        }

        private void OnClickQuit()
        {
            Log("Quit Click");
            _LevelManager.QuitLevel();
        }
    }
}
