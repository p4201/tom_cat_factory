﻿using Game.Core.Components.Timing;
using Game.Core.Components.UI;
using Game.Core.Components.UI.Screens;
using Game.Scripts.Level.Management;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.Scripts.Level.UI.Views
{
    [RequireComponent(typeof(ScreenController))]
    public class UIGameScreen : ScreenBehaviour
    {
        [Inject] private readonly LevelManager _LevelManager;

        [Header("Components")]
        [SerializeField] private Button _PauseButton = null;
        [SerializeField] private UniversalText _TimerText = null;
        [SerializeField] private TimerComponent _TimerComponent = null;

        ////////////////////////////////////////////////

        private void OnEnable()
        {
            _TimerComponent.onUpdate.AddListener(OnUpdate);
            _PauseButton.onClick.AddListener(OnClickPause);
        }

        private void OnDisable()
        {
            _TimerComponent.onUpdate.RemoveListener(OnUpdate);
            _PauseButton.onClick.RemoveListener(OnClickPause);
        }

        ////////////////////////////////////////////////

        private void OnUpdate()
        {
            _TimerText.SetText(_TimerComponent.secondsLeft);
        }

        private void OnClickPause()
        {
            Log("Pause Click");
            _LevelManager.PauseLevel();
        }
    }
}
