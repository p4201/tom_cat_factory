﻿using System.Collections.Generic;
using Game.Core.Components.UI.Screens;
using Game.Scripts.Definitions.Valuables;
using Game.Scripts.Level.Management;
using Game.Scripts.Level.UI.Objects;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.Scripts.Level.UI.Views
{
    [RequireComponent(typeof(ScreenController))]
    public class UILevelCompleteScreen : ScreenBehaviour
    {
        [Inject] private readonly LevelManager _LevelManager;

        [Header("Components")]
        [SerializeField] private UIStarView[] _Stars = new UIStarView[0];
        [SerializeField] private UIRewardsContainer _Rewards = null;

        [SerializeField] private Button _NextLevelButton = null;
        [SerializeField] private Button _QuitButton = null;

        ////////////////////////////////////////////////

        private void OnEnable()
        {
            _NextLevelButton.onClick.AddListener(OnClickNextLevel);
            _QuitButton.onClick.AddListener(OnClickQuit);
        }

        private void OnDisable()
        {
            _NextLevelButton.onClick.RemoveListener(OnClickNextLevel);
            _QuitButton.onClick.RemoveListener(OnClickQuit);
        }

        ////////////////////////////////////////////////

        public void SetRewards(IReadOnlyList<CountableValuableConfig> rewards)
        {
            foreach (var reward in rewards)
            {
                if (reward.valuable.type == ValuableType.Star)
                    DisplayStars(reward.count);
                else
                    AddReward(reward);
            }
        }

        ////////////////////////////////////////////////

        private void DisplayStars(int starsCount)
        {
            Log($"Display {starsCount} stars");

            for (var i = 0; i < _Stars.Length; i++)
                _Stars[i].SetActive(i < starsCount);
        }

        private void AddReward(CountableValuableConfig reward)
        {
            Log($"Add reward: {reward.count} {reward.valuable.name}");
            _Rewards.SetReward(reward.valuable.icon, reward.count);
        }

        ////////////////////////////////////////////////

        private void OnClickNextLevel()
        {
            Log("Next Level Click");
            _LevelManager.StartNextLevel();
        }

        private void OnClickQuit()
        {
            Log("Quit Click");
            _LevelManager.QuitLevel();
        }
    }
}
