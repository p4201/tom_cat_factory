﻿using Game.Core.Components.UI.Screens;
using Game.Scripts.Level.Management;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.Scripts.Level.UI.Views
{
    [RequireComponent(typeof(ScreenController))]
    public class UIPauseScreen: ScreenBehaviour
    {
        [Inject] private readonly LevelManager _LevelManager;

        [Header("Components")]
        [SerializeField] private Button[] _CloseButtons = null;
        [SerializeField] private Button _RestartButton = null;
        [SerializeField] private Button _QuitButton = null;

        ////////////////////////////////////////////////

        private void OnEnable()
        {
            _RestartButton.onClick.AddListener(OnRestartClick);
            _QuitButton.onClick.AddListener(OnQuitClick);

            foreach (var button in _CloseButtons)
                button.onClick.AddListener(OnCloseClick);
        }

        private void OnDisable()
        {
            _RestartButton.onClick.RemoveListener(OnRestartClick);
            _QuitButton.onClick.RemoveListener(OnQuitClick);

            foreach (var button in _CloseButtons)
                button.onClick.RemoveListener(OnCloseClick);
        }

        ////////////////////////////////////////////////

        private void OnRestartClick()
        {
            Log("Quit Click");
            _LevelManager.RestartLevel();
        }

        private void OnQuitClick()
        {
            Log("Quit Click");
            _LevelManager.QuitLevel();
        }

        private void OnCloseClick()
        {
            Log("Close Click");
            _LevelManager.UnpauseLevel();
        }
    }
}
