﻿using Game.Core.Components;
using Game.Core.Components.UI;
using Game.Core.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Level.UI.Objects
{
    public class UIRewardView : LoggedMonoBehaviour, IResettable
    {
        [SerializeField] private Image _Icon = null;
        [SerializeField] private UniversalText _Text = null;

        ////////////////////////////////////////////////

        public void Init(Sprite icon, int count)
        {
            Log("Init");
            SetActive(true);

            _Icon.sprite = icon;
            _Text.SetText(count);
        }

        public void Reset()
        {
            Log("Reset");
            SetActive(false);

            _Icon.sprite = null;
            _Text.SetText(string.Empty);
        }

        ////////////////////////////////////////////////
        private void SetActive(bool status) =>
            gameObject.SetActive(status);
    }
}
