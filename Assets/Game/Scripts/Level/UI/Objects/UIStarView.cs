﻿using Game.Core.Components;
using Game.Scripts.Level.Definitions.Level;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.Scripts.Level.UI.Objects
{
    public class UIStarView : LoggedMonoBehaviour
    {
        [Inject] private readonly UIRewardsViewConfig _Config;

        [Header("Components")]
        [SerializeField] private Image _Image;

        public bool isActive { get; private set; }

        ////////////////////////////////////////////////

        public void SetActive(bool status)
        {
            Log(status ? "Active" : "Inactive");
            isActive = status;
            _Image.sprite = _Config.GetStarIcon(status);
        }
    }
}
