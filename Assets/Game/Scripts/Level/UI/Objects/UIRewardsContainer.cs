﻿using System.Collections.Generic;
using Game.Core.Components;
using Game.Core.DI;
using Game.Scripts.Level.Definitions.Level;
using UnityEngine;
using Zenject;

namespace Game.Scripts.Level.UI.Objects
{
    public class UIRewardsContainer : LoggedMonoBehaviour
    {
        [Inject] private readonly UIRewardsViewConfig _Config;
        [Inject] private readonly PrefabInstanceCreator _Creator;

        [SerializeField] private Transform _Container = null;
        [SerializeField] private List<UIRewardView> _Rewards = new List<UIRewardView>();

        ////////////////////////////////////////////////

        private void OnValidate()
        {
            if (_Container == null)
                _Container = transform;

            if (_Rewards == null)
                _Rewards = new List<UIRewardView>();
        }

        private void Start() => UpdateStatus();

        ////////////////////////////////////////////////

        public void SetReward(Sprite icon, int count)
        {
            var reward = _Creator.Instantiate(_Config.rewardPrefab, _Container);
            reward.Init(icon, count);
            _Rewards.Add(reward);

            UpdateStatus();
        }

        public void ClearRewards()
        {
            foreach (var reward in _Rewards)
                Destroy(reward.gameObject);

            _Rewards.Clear();
            UpdateStatus();
        }

        ////////////////////////////////////////////////

        private void UpdateStatus() =>
            gameObject.SetActive(_Rewards.Count > 0);
    }
}
