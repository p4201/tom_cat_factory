﻿using Game.Core.Components.Timing;
using Game.Core.Extensions;
using Game.Scripts.Level.Definitions.Level;
using UnityEngine;

namespace Game.Scripts.Level.Objects.Transporter
{
    public class TransporterController : LevelObjectController
    {
        [Header("Components")]
        [SerializeField] private StopwatchComponent _Stopwatch = null;

        [Header("Events")]
        [SerializeField] private UnityEventVector2 _OnTransporting = new UnityEventVector2();

        private TransporterConfig _config;

        public float distance { get; private set; }
        public float time => _Stopwatch.currentTime;
        public UnityEventVector2 onTransporting => _OnTransporting;

        ////////////////////////////////////////////////

        private void OnEnable() =>
            _Stopwatch.onUpdate.AddListener(OnUpdate);

        private void OnDisable() =>
            _Stopwatch.onUpdate.RemoveListener(OnUpdate);

        ////////////////////////////////////////////////

        public void Init(TransporterConfig config)
        {
            Log("Init");
            ResetDistance();
            _Stopwatch.Reset();

            _config = config;
        }

        public override void Reset()
        {
            Log("Reset");
            ResetDistance();
            _Stopwatch.Reset();
        }

        private void ResetDistance() => distance = 0;

        ////////////////////////////////////////////////

        [ContextMenu("Start Transport")]
        public void StartTransport()
        {
            Log("Start Transport");
            _Stopwatch.StartCounting();
        }

        [ContextMenu("Pause Transport")]
        public void PauseTransport()
        {
            Log("Pause Transport");
            _Stopwatch.PauseCounting();
        }

        [ContextMenu("Stop Transport")]
        public void StopTransport()
        {
            Log("Stop Transport");
            ResetDistance();
            _Stopwatch.StopCounting();
        }

        ////////////////////////////////////////////////

        private void OnUpdate(float dt)
        {
            var direction = _config.direction;
            var distanceDiff = _config.speed.speed.GetCurrent(_Stopwatch.currentTime) * dt;

            distance += distanceDiff;

            _OnTransporting?.Invoke(direction * distanceDiff);
        }
    }
}
