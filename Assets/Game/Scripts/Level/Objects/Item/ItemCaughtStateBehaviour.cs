﻿using UnityEngine;

namespace Game.Scripts.Level.Objects.Item
{
    public class ItemCaughtStateBehaviour : StateMachineBehaviour
    {
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            var item = GetItemController(animator);
            item.sortingLayerChanger.SetNextLayer();
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            var item = GetItemController(animator);
            item.sortingLayerChanger.SetNextLayer();
            item.CallFinalize();
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) { }

        public override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) { }

        public override void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) { }

        ////////////////////////////////////////////////

        private ItemController GetItemController(Animator animator) =>
            animator.gameObject.GetComponent<ItemController>();
    }
}
