﻿using Game.Scripts.Components;
using Game.Scripts.Level.Extensions;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Scripts.Level.Objects.Item
{
    [RequireComponent(typeof(Collider2D))]
    public class ItemsCatcher : ObjectsCatcher<ItemController>
    {
        [SerializeField] private UnityEventItemController _OnCaught = new UnityEventItemController();

        public override UnityEvent<ItemController> onCaught => _OnCaught;
    }
}
