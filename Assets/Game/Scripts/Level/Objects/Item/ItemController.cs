﻿using Game.Core.Components;
using Game.Core.Components.UserInput;
using Game.Scripts.Components;
using Game.Scripts.Level.Definitions.Level;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Scripts.Level.Objects.Item
{
    [RequireComponent(typeof(Collider2D))]
    public class ItemController : LevelObjectController
    {
        [Header("Components")]
        [SerializeField] private ItemAnimator _Animator = null;
        [SerializeField] private SpriteRenderer _Sprite = null;
        [SerializeField] private MoveComponent _MoveComponent = null;
        [SerializeField] private PointerTrigger2D _PointerTrigger = null;
        [SerializeField] private SortingLayerChanger _SortingLayerChanger = null;

        [Header("Events")]
        [SerializeField] private UnityEvent _OnFall = new UnityEvent();
        [SerializeField] private UnityEvent _OnClick = new UnityEvent();
        [SerializeField] private UnityEvent _OnCaught = new UnityEvent();

        public ItemConfig config => _model?.config;
        public SortingLayerChanger sortingLayerChanger => _SortingLayerChanger;

        public UnityEvent onFall => _OnFall;
        public UnityEvent onClick => _OnClick;
        public UnityEvent onCaught => _OnCaught;

        private ItemView _view;
        private ItemModel _model;

        ////////////////////////////////////////////////

        private void OnDestroy()
        {
            Unsubscribe();
        }

        ////////////////////////////////////////////////

        public void Init(ItemConfig itemConfig, FallingConfig fallingConfig)
        {
            if (_view == null) _view = new ItemView(_Animator);
            else _view.Init(_Animator);

            if (_model == null) _model = new ItemModel(itemConfig);
            else _model.Init(itemConfig);

            _PointerTrigger.SetActive(true);
            _Sprite.sprite = itemConfig.sprite;
            _MoveComponent.Init(transform, fallingConfig.speed, fallingConfig.direction);

            Unsubscribe();
            Subscribe();
        }

        public override void Reset()
        {
            _view.Reset();
            _model.Reset();
            _Animator.Reset();
            _PointerTrigger.SetActive(true);
        }

        ////////////////////////////////////////////////

        private void Subscribe()
        {
            _PointerTrigger.onPointerClick.AddListener(OnPointerClick);
        }

        private void Unsubscribe()
        {
            _PointerTrigger.onPointerClick.RemoveListener(OnPointerClick);
        }

        ////////////////////////////////////////////////

        [ContextMenu("Fall")]
        public void Fall()
        {
            if (_model.falling)
                return;

            Log("Fall");

            _view.Fall();
            _model.Fall();
            _MoveComponent.StartMove();
            _PointerTrigger.SetActive(false);

            _OnFall?.Invoke();
        }

        public void Caught()
        {
            if (_model.caught)
                return;

            Log("Caught");
            _view.Caught();
            _model.Caught();

            _OnCaught?.Invoke();
        }

        ////////////////////////////////////////////////

        private void OnPointerClick()
        {
            Log("Click");
            _view.Click();
            _OnClick?.Invoke();

            Fall();
        }
    }
}
