﻿
namespace Game.Scripts.Level.Objects.Item
{
    public class ItemView : LevelObjectView
    {
        private ItemAnimator _animator;

        ////////////////////////////////////////////////

        public ItemView(ItemAnimator animator) => Init(animator);

        public void Init(ItemAnimator animator)
        {
            Log("Init");
            _animator = animator;
        }

        public override void Reset()
        {
            Log("Reset");
            _animator.Reset();
        }

        ////////////////////////////////////////////////

        public void Fall()
        {
            Log("Fall");
            _animator.PlayFall();
        }

        public void Click()
        {
            Log("Click");
            _animator.PlayClick();
        }

        public void Caught()
        {
            Log("Caught");
            _animator.PlayCaught();
        }
    }
}
