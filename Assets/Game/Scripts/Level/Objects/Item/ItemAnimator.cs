﻿using Game.Core.Components;
using Game.Core.Interfaces;
using UnityEngine;

namespace Game.Scripts.Level.Objects.Item
{
    [RequireComponent(typeof(Animator))]
    public class ItemAnimator : LoggedMonoBehaviour, IResettable
    {
        [Header("Components")]
        [SerializeField] private Animator _Animator = null;

        [Header("Parameters")]
        [SerializeField] private string _FallParameterName = "";
        [SerializeField] private string _ClickParameterName = "";
        [SerializeField] private string _CaughtParameterName = "";

        ////////////////////////////////////////////////

        public void Reset()
        {
            Log("Reset");
            _Animator.ResetTrigger(_ClickParameterName);
            _Animator.ResetTrigger(_CaughtParameterName);
        }

        ////////////////////////////////////////////////

        public void PlayFall()
        {
            Log("Fall");
            _Animator.SetTrigger(_FallParameterName);
        }

        public void PlayClick()
        {
            Log("Play Click");
            _Animator.SetTrigger(_ClickParameterName);
        }

        public void PlayCaught()
        {
            Log("Play Caught");
            _Animator.SetTrigger(_CaughtParameterName);
        }
    }
}
