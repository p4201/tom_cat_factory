﻿using Game.Scripts.Level.Definitions.Level;

namespace Game.Scripts.Level.Objects.Item
{
    public class ItemModel : LevelObjectModel
    {
        public ItemConfig config { get; private set; }
        public bool falling { get; private set; }
        public bool caught { get; private set; }

        ////////////////////////////////////////////////

        public ItemModel(ItemConfig config) => Init(config);

        public void Init(ItemConfig config)
        {
            Log("Init");
            caught = false;
            falling = false;
            this.config = config;
        }

        public override void Reset()
        {
            Log("Reset");
            caught = false;
            falling = false;
        }

        ////////////////////////////////////////////////

        public void Caught()
        {
            if (caught)
                return;

            Log("Caught");
            caught = true;
            falling = false;
        }

        public void Fall()
        {
            if (falling)
                return;

            Log("Fall");
            falling = true;
        }
    }
}
