﻿using Game.Core.Components;
using Game.Core.Components.UI;
using Game.Core.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Level.Objects.Box
{
    public class BoxTargetArea : LoggedMonoBehaviour, IResettable
    {
        [Header("Components")]
        [SerializeField] private Image _TargetIcon;
        [SerializeField] private CompositeText _TargetCounter;

        private const int _CURRENT_PROGRESS_INDEX = 0;
        private const int _CAPACITY_PROGRESS_INDEX = 1;

        ////////////////////////////////////////////////

        public void Reset()
        {
            Log("Reset");
            SetProgress(0);
        }

        ////////////////////////////////////////////////

        public void SetTarget(Sprite icon, int capacity)
        {
            Log($"Set target {icon.name}");
            _TargetIcon.sprite = icon;
            _TargetCounter.SetPart(capacity, _CAPACITY_PROGRESS_INDEX);
        }

        public void SetProgress(int current)
        {
            Log($"Set progress: {current}");
            _TargetCounter.SetPart(current, _CURRENT_PROGRESS_INDEX);
        }

        public void SetProgress(int current, int capacity)
        {
            Log($"Set progress: {current}/{capacity}");
            _TargetCounter.SetParts(current, capacity);
        }
    }
}
