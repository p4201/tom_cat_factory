﻿using UnityEngine;

namespace Game.Scripts.Level.Objects.Box
{
    public class BoxView : LevelObjectView
    {
        private BoxAnimator _animator;
        private BoxTargetArea _targetArea;

        ////////////////////////////////////////////////

        public BoxView(BoxAnimator animator, BoxTargetArea targetArea) =>
            Init(animator, targetArea);

        public void Init(BoxAnimator animator, BoxTargetArea targetArea)
        {
            Log("Init");
            _animator = animator;
            _targetArea = targetArea;
        }

        public override void Reset()
        {
            Log("Reset");
            _animator.Reset();
            _targetArea.Reset();
        }

        ////////////////////////////////////////////////

        public void SetTarget(Sprite targetIcon, int capacity)
        {
            Log("Set target");
            _targetArea.SetTarget(targetIcon, capacity);
        }

        public void Click()
        {
            Log("Click");
            _animator.PlayClick();
        }

        public void Catch(int current)
        {
            Log("Catch");
            _animator.PlayCatch();
            _targetArea.SetProgress(current);
        }

        public void Complete()
        {
            Log("Complete");
            _animator.PlayComplete();
        }
    }
}
