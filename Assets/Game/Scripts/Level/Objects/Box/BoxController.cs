﻿using Game.Core.Components.UserInput;
using Game.Scripts.Level.Definitions.Level;
using Game.Scripts.Level.Extensions;
using Game.Scripts.Level.Objects.Item;

using UnityEngine;
using UnityEngine.Events;

namespace Game.Scripts.Level.Objects.Box
{
    public class BoxController : LevelObjectController
    {
        [Header("Components")]
        [SerializeField] private BoxAnimator _Animator = null;
        [SerializeField] private ItemsCatcher itemsCatcher = null;
        [SerializeField] private BoxTargetArea _TargetArea = null;
        [SerializeField] private PointerTrigger2D _PointerTrigger = null;

        [Header("Events")]
        [SerializeField] private UnityEvent _OnCatch = new UnityEvent();
        [SerializeField] private UnityEvent _OnComplete = new UnityEvent();
        [SerializeField] private UnityEventBoxController _OnClick = new UnityEventBoxController();

        public bool isCompleted => _model.isCompleted;

        public UnityEvent onCatch => _OnCatch;
        public UnityEvent onComplete => _OnComplete;
        public UnityEventBoxController onClick => _OnClick;

        private BoxView _view;
        private BoxModel _model;
        private ItemConfig _targetItem;

        ////////////////////////////////////////////////

        private void OnDestroy() =>
            Unsubscribe();

        ////////////////////////////////////////////////

        public void Init(ItemConfig targetItem, int itemsCapacity)
        {
            if (_view == null) _view = new BoxView(_Animator, _TargetArea);
            else _view.Init(_Animator, _TargetArea);

            if (_model == null) _model = new BoxModel(itemsCapacity);
            else _model.Init(itemsCapacity);

            _targetItem = targetItem;
            _view.SetTarget(targetItem.icon, itemsCapacity);

            Unsubscribe();
            Subscribe();
        }

        public override void Reset()
        {
            Log("Reset");
            _view.Reset();
            _model.Reset();
        }

        ////////////////////////////////////////////////

        private void Subscribe()
        {
            itemsCatcher.onCaught.AddListener(OnItemCaught);
            _PointerTrigger.onPointerClick.AddListener(OnPointerClick);
        }

        private void Unsubscribe()
        {
            itemsCatcher.onCaught.RemoveListener(OnItemCaught);
            _PointerTrigger.onPointerClick.RemoveListener(OnPointerClick);
        }

        ////////////////////////////////////////////////

        private void OnPointerClick()
        {
            if (isCompleted)
                return;

            Log("Click");
            _view.Click();

            _OnClick?.Invoke(this);
        }

        private void OnItemCaught(ItemController item)
        {
            if (_targetItem != item.config)
            {
                Log("Can't catch wrong item!");
                return;
            }

            if (_model.isCompleted)
            {
                Log("Box is already completed!");
                return;
            }

            Log("Catch");
            _model.IncreaseCounter();
            _view.Catch(_model.itemCounter);

            _OnCatch?.Invoke();
            item.Caught();
            TryComplete();
        }

        private void TryComplete()
        {
            if (!_model.canBeCompleted)
                return;

            Log("Complete");
            _view.Complete();
            _model.Complete();

            _OnComplete?.Invoke();
        }
    }
}
