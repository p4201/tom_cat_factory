﻿using Game.Core.Components;
using Game.Core.Interfaces;
using UnityEngine;

namespace Game.Scripts.Level.Objects.Box
{
    [RequireComponent(typeof(Animator))]
    public class BoxAnimator : LoggedMonoBehaviour, IResettable
    {
        [Header("Components")]
        [SerializeField] private Animator _Animator = null;

        [Header("Parameters")]
        [SerializeField] private string _ClickParameterName = "";
        [SerializeField] private string _CatchParameterName = "";
        [SerializeField] private string _CompleteParameterName = "";

        ////////////////////////////////////////////////

        public void Reset()
        {
            Log("Reset");
            _Animator.ResetTrigger(_ClickParameterName);
            _Animator.ResetTrigger(_CatchParameterName);
            _Animator.ResetTrigger(_CompleteParameterName);
        }

        ////////////////////////////////////////////////

        public void PlayClick()
        {
            Log("Play Click");
            _Animator.SetTrigger(_ClickParameterName);
        }

        public void PlayCatch()
        {
            Log("Play Catch");
            _Animator.SetTrigger(_CatchParameterName);
        }

        public void PlayComplete()
        {
            Log("Play Complete");
            _Animator.SetTrigger(_CompleteParameterName);
        }
    }
}
