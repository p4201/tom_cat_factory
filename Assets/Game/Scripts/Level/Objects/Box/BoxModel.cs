﻿
namespace Game.Scripts.Level.Objects.Box
{
    public class BoxModel : LevelObjectModel
    {
        public int itemsCapacity { get; private set; }
        public int itemCounter { get; private set; }
        public bool isCompleted { get; private set; }
        public bool canBeCompleted => !isCompleted && itemCounter >= itemsCapacity;

        ////////////////////////////////////////////////

        public BoxModel(int itemsCapacity) => Init(itemsCapacity);

        public void Init(int itemsCapacity)
        {
            Log("Init");
            ResetCounters();
            this.itemsCapacity = itemsCapacity;
        }

        public override void Reset()
        {
            Log("Reset");
            ResetCounters();
        }

        private void ResetCounters()
        {
            itemCounter = 0;
            isCompleted = false;
        }

        ////////////////////////////////////////////////

        public void IncreaseCounter()
        {
            Log("Increase Counter");
            itemCounter++;
        }

        public void Complete()
        {
            Log("Complete");
            isCompleted = true;
        }
    }
}
