﻿using Game.Scripts.Level.Objects.Item;
using UnityEngine;

namespace Game.Scripts.Level.Objects.Container
{
    public class ContainerController : LevelObjectController
    {
        [SerializeField] private ItemController _Item = null;
        [SerializeField] private Transform _Pivot = null;

        private Transform _itemParent;
        private Transform _transformCache;
        private Transform _transform => _transformCache ?? ( _transformCache = transform );

        public bool isFree => _Item == null;
        public ItemController item => _Item;

        ////////////////////////////////////////////////

        public override void Reset()
        {
            Log("Reset");
            ReleaseItem();
        }

        ////////////////////////////////////////////////

        public void SetItem(ItemController item)
        {
            Log("Set item");

            if (!isFree)
                ReleaseItem();

            _Item = item;
            AdoptItem(item);
            SetItemToPivot(item);
            item.onFall.AddListener(ReleaseItem);
        }

        public void ReleaseItem()
        {
            if (isFree)
            {
                Log("Can't release item - it's null!");
                return;
            }

            item.onFall.RemoveListener(ReleaseItem);
            EvictItem(_Item);
            _Item = null;

            Log("Release item");
        }

        public void Move(Vector2 diffPosition)
        {
            _transform.localPosition += (Vector3) diffPosition;
        }

        ////////////////////////////////////////////////

        private void SetItemToPivot(ItemController item)
        {
            var itemTransform = item.transform;
            var offset = _Pivot.position - itemTransform.position;
            itemTransform.position += offset;
        }

        private void AdoptItem(ItemController item)
        {
            var itemTransform = item.transform;
            _itemParent = itemTransform.parent;
            itemTransform.SetParent(transform);
        }

        private void EvictItem(ItemController item)
        {
            item.transform.SetParent(_itemParent);
        }
    }
}
