﻿using Game.Scripts.Components;
using Game.Scripts.Level.Extensions;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Scripts.Level.Objects.Container
{
    [RequireComponent(typeof(Collider2D))]
    public class ContainersCatcher : ObjectsCatcher<ContainerController>
    {
        [Header("Events")]
        [SerializeField] private UnityEventContainerController _OnCaught = new UnityEventContainerController();

        public override UnityEvent<ContainerController> onCaught => _OnCaught;
    }
}
