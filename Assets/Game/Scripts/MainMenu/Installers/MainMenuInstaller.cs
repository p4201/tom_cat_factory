﻿using Game.Core.DI;
using Zenject;

namespace Game.Scripts.MainMenu.Installers
{
    public class MainMenuInstaller : MonoInstaller<MainMenuInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<PrefabInstanceCreator>().FromInstance(new PrefabInstanceCreator(Container)).AsSingle();
        }
    }
}
