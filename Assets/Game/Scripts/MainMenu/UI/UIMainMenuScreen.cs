﻿using Game.Core.Components.UI.Screens;
using Game.Scripts.Management;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.Scripts.MainMenu.UI
{
    [RequireComponent(typeof(ScreenController))]
    public class UIMainMenuScreen : ScreenBehaviour
    {
        [Inject] private readonly ScenesManager _ScenesManager;

        [SerializeField] private Button _PlayButton = null;

        ////////////////////////////////////////////////

        private void OnEnable() =>
            _PlayButton.onClick.AddListener(OnPlayClicked);

        private void OnDisable() =>
            _PlayButton.onClick.RemoveListener(OnPlayClicked);

        ////////////////////////////////////////////////

        private void OnPlayClicked()
        {
            Log("Play Clicked");
            _ScenesManager.LoadMetaScene();
        }
    }
}
